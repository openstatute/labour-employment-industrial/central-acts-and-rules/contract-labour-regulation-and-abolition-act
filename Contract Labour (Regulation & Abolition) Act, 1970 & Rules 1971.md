Contract Labour (Regulation & Abolition) Act, 1970
==================================================

Contract Labour (Regulation & Abolition) Act, 1970 & Rules 1971

\[No.37 OF 1970\]  
_\[September 5, 1970\]_  
**CONTENTS**             
**[CHAPTER I](#Chapter I)**

#### **Sections**                                   **_[Preliminary](#Preliminary)_**

1\. [Short title, extent and commencement](#Short title, extent, commencement and application)  
2\. [Definitions](#Definitions)  
**[CHAPTER II](#ChapterII)**  
_[The Advisory Boards](#Advisory Board)_  
3\. [Central Advisory Board](#Central Advisory Board)  
4\. [State Advisory Board](#State Advisor Board)  
5\. [Power to Constitute Committees](#Power to constitute Committees)  
**[CHAPTER III](#Chapter III)**  
_[Registration of Establishments Employing Contract Labour](#Registration of Establishment)_  
6\. [Appointment of registering officers](#Appointment of registering officers)  
7\. [Registration of certain establishments](#Registration of certain establishment)  
8\. [Revocation of registration in certain cases](#Revocation of registration in certain cases)  
9\. [Effect of non-registration](#Effect of non-registration)  
10.[Prohibition of employment of contract labour](#Prohibition of employment of contract labour)  
**[CHAPTER IV](#Chapter IV)**  
_[Licensing of Contractors](#LICENSING OF CONTRACTORS)_  
11\. [Appointment of licensing officers](#Appointment of licensing officers)  
12. [Licensing of contractors](#Licensing of contractors.)  
13\. [Grant of licences](#Grant of licences)  
14\. [Revocation, suspension and amendment of licences](#Revocation, suspension and amendment of licences)  
15\. [Appeal](#Appeal)  
**[CHAPTER V](#CHAPTER V)**  
_[Welfare and Health of Contract Labour](#WELFARE AND HEALTH OF CONTRACT LABOUR)_  
16\. [Canteens](#Canteens)  
17\. [Rest-rooms](#Rest-rooms)  
18\. [Other-facilities](#Other facilities)  
19\. [First-aid facilities](#First-aid facilities)  
20. [Liability of principal employer in certain cases](#Liability of principal employer in certain cases)  
21\. [Responsibility for payment of wages](#Responsibility for payment of wages)  
**[CHAPTER VI](#CHAPTER VI)**  
_[Penalties and Procedure](#PENAL TIES AND PROCEDURE)_  
22\. [Obstructions](#Obstructions)  
23\. [Contravention of provisions regarding employment of contract labour](#Contravention of provisions)  
24\. [Other offences.](#Other offences)  
25\. [Offences by companies](#Offences by companies.)  
26. [Cognizance of offences](#Cognizance or offences).  
27\. [Limitation of prosecution](#Limitation or prosecution)  
**[CHAPTER VII](#CHAPTER VII)**  
_[Miscellaneous](#MISCELLANEOUS)_  
28\. I[nspecting staff](#Inspecting staff)  
29. [Registers and other records to be maintained](#Registers and other records to be maintained.)  
30\. [Effect of laws and agreements inconsistent with this Act](#Effect of laws and agreements inconsistent with this Act.)  
31\. [Power to exempt in special cases](#Power to exempt in special cases)  
32\. [Protection of action taken under this Act](#Protection of action taken under this Act.-)  
33\. [Power to give directions](#Power to give directions)  
34\. [Power to remove difficulties](#Power to remove difficulties)  
35\. [Power to make rules](#Power to make rules)  
36\. [Contract Labour (Regulation & Abolition)](#Central Rules) [Central Rules, 1971](#Central Rules)

_An Act to regulate the employment of contract labour in certain establishments and to provide for its abolition in certain circumstances and for matters connected therewith_  
 Be enacted by Parliament in the Twenty-first Year of the Republic of India as follows :   
CHAPTER I  
_PRELIMINARY_   
**1\. Short title, extent, commencement and application.-**

(1) This Act may be called the Contract Labour (Regulation and Abolition) Act, 1970.  
(2) It extends to the whole of India.  
(3) It shall come into force' on such date- as the Central Government may, by notification in the Official Gazette, appoint and different dates may be appointed for different provisions of this Act. -  
(4) It applies-  
(a) to every establishment in which twenty or more workmen, art employed or were employed on any day of the preceding twelve months as contract labour.  
(b) to every contractor who employs or who employed on any day of the preceding twelve months twenty or more workmen.  
Provided that the appropriate Government may, after giving not less than two months' notice of its intention so to do, by notification in the Official Gazette, apply the provisions of this Act to any establishment or contractor employing such number of workmen less than twenty as may be specified in the notification.  
(5) (a) It shall not apply to establishments in which work only of an intermittent or casual nature is performed.  
(b) If a question arises whether work performed in an establishment is of an intermittent or casual nature, the appropriate Government shall decide that question after consultation with the Central Board or, as the case may be, a State Board, and its decision shall be final.  
_Explanation.-_For the purpose of this sub-section, work performed in an establishment shall not be deemed to be of an intermittent nature-  
(i) if it was, performed for more than one hundred and twenty days in the preceding twelve months, or  
(ii) if it is of a seasonal character and is performed for more than sixty days in a year.  
**Note**.-The Act carne into force on 10th February, 1971, vide Noti No. G. S. R. 190, dated 1st February, 1971, and published in Gazette of India, Extra., Part II, Section 3(i), dated February 10, 1971, p.173. -   
**Validity**.-Object and purpose of the Act-Application of the Act to pending construction works does not amount to unreasonable restriction on the right under Act 19(1) (g). The whole statute is constitutional and valid. Gammon India Ltd. v. Union of India, 1974 SCC (L & S) 252.  
S.1- **Applicability-** Where the dispute relates to service conditions of the workmen engaged in the factory canteen maintained by the company and there is no question of abolition of contract labour, the dispute can be referred to the industrial Tribunal for adjudication _Indian Explosives Ltd. v. State of u. P._,  (1981) 1 LLJ 423 (All H.C.)  
[Top](#Section)  
**NOTES**  
 Act does not violate Arts. 14 and 15_. Gammon India Ltd. v. Union of India_, (1974) 1 SCC 596,603: 1974 SCC (L & S) 252.  
2\. **Definitions**.-(1) In this Act, unless the context otherwise  requires,-  
(a) 'appropriate Government' means,-  
(i) in relation to an establishment in respect of which the appropriate Government under the Industrial Disputes Act, 1947 (14 of 1947), is the Central Government;  
(ii) in relation to any other establishment, the Government of the State in which that other establishment is situated.

(b) a workman shall be deemed to be employed as "contract labour" in or in connection with the work-of:-an establishment when he is hired in or in connection with such work by or through a contractor, with or without the knowledge of the principal employer.  
(c) "contractor", in relation to an establishment, means- a person who undertakes to produce  a given result for the establishment, other than a mere supply of goods or articles of   manufacture to such establishment, through contract labour or who supplies contract   labour for any work of the establishment and includes a sub-contractor ;   
[Top](#Section)  
**NOTES**  
S.2 (1)(c)-Contractor engaged for S. 2(c) covers construction of building. _Gammon India Ltd. v. Union of India,_ (1974) 1 SCC 596: 1971 SCC (L & S) 252.  
Ss. 2(1)(c) & 12-Where a person undertook to collect and manufacture quarry products for and on behalf of railways by engaging workmen to carry out his contract works under the railway establishment, the workmen employed by him form such work are to be deemed as "contract labour" as provided under S.2 (1)(b). The supply of such quarry products would produce a given result for the establishment, thus he fulfils all requirements of a "contractor" under S. 2 (1)(c) and therefore, is obliged to take licence under S. 12(1_). H.C. Bathra v. Union of India_, 1976 Lab IC 1199 (Gauhati).   
(d) "controlled industry" means any industry the control of which by the Union has been declared by any Central Act to be expedient client in the public interest;  
(e) "establishment" means-  
(i) any office or department of the Government or a local authority, or -  
(ii) any place where any industries, trade, business, manufacture or occupation is carried on;   
**NOTES**  
S.2 (1)(c)-'Contractor' is one who supplies contract labour to an establishment undertaking to produce a given result for it. He hires labour in connection with the work of an establishment. _State of Gujarat v. Vogue Garments_, (1983) 1 LLJ 255: 1983 Lab IC 129 (Guj HC).   
S. 2 (2) (C)-Sub-contractors or 'piece wagers', are contractors. _Labourers Working on Salal Hydro Project v. State of J & K_, (1983)/2 SCG 181.   
S.2 (1)(e)(ii)-A ship or vessel in which repair work is carried on is a place and an "establishment" within the meaning of S. 2 (I) (e) (ii). The work site or place may or may not belong to the principal employer, but that will not stand in the way of application of the Act or in holding that a particular place or work site where industry, trade, business, manufacture or occupation is carried on is not an establishment. _Lionel Edwards Led. v. Labour Enforcement Officer_, (1977) 51 FJR 199 (Cal).   
S.2(1)(e)(ii)-Any object for the time being covering the surface and where industry, trade, business, manufacture or occupation is carried on would be a place under   
S.2(1)(e)(ii). A ship anchored or berthed in a port would be a work site and the workmen employed for loading and unloading of the cargo, security, repairs to the ship would be all in connection with the business or trade. The Docks in which a ship may be berthed is controlled by the Port Authorities and the ship owners' agents would be unable to provide facilities for canteens, rest rooms etc. But these defects cannot be ground for totally excluding a ship in a port from the ambit of "establishment_". Lionel Edwards Ltd. v, Labour Enforcement Officer_, (1978) 53 FJR 116 (Cal DB).  
(f) "prescribed" means prescribed by rules made under this Act;  
(g) "principal employer" means-

                    (i) in relation to any office or department of the Government  or a local authority, the    head of that office or department or such other officer as "'the Government or the local authority, as the case may be, may specify in this behalf,  
                    (ii) in a factory, the owner or occupier of the factory and where a person has been named as the manager of the factory under the Factories Act, 1948 (63 of 1948), the person so named.  
 **NOTES**  
 The word 'Occupier' has been defined in Section 2(n) of the Factories Act; 1948 as under:  
 "Occupier" of a factory means the person who has ultimate control over the affairs of the factory, and where the said affairs are entrusted to a managing agent, such agent shall be deemed to be the occupier of the factory.  
(iii) in a mine, the owner or agent of the mine and where a person has been named as the manager of the mine, the person so named,  
(iv) in any other establishment, any person responsible for the supervision and control of the establishment.  
 _Explanation.-_For the purpose of sub-clause (iii) of this clause expressions mine", "owner" and "agent" shall have the meanings respectively  assigned   clause (j), clause (1) and clause (c) of sub-section (1) of Section 2 of the 'Mines Act, 1952 (35 of 1952) ;  
   
(h)  "wages" shall have the meaning assigned to it in clause (vi) of Section 2 of the Payment of Wages Act, 1936 (4 of 1936);   
(i) "workman" means any person employed in or in connection with the work of any establishment to do any skilled, semi-skilled or un-skilled manual, supervisory, technical or clerical work for hire or reward, whether the terms of employment be express or implied, but does not include any such person-  
(A)    who is employed mainly in a managerial or administrative capacity; or    
(B)    who, being employed in a supervisory capacity draws wages exceeding five hundred rupees per mensem or exercises, either by the nature of the duties attached to the office or by' reason of the powers vested in him? functions mainly of a managerial nature; or   
(C)    who is an out worker, that is to say, a person to whom any articles and materials are given out by or on behalf of' the principal employer to be made up, cleaned, washed, altered, ornamented, finished, repaired, adapted or otherwise processed for sale for the purposes of the .trade or  business of the principal employer and the process is to be carried out either in the home of the out-worker or in some other premises, not being premises under the control and management of the principal employer.   
(2) Any reference in this Act to a law which is not in force in the State of Jammu and Kashmir shall, In relation to that State, be construed as a reference to the corresponding law, if any, in force in that State.   
**Note** :-Held, the fact the work of the contractor is away from the establishment does not make it out of "work of any establishment" in S. 2(1)(c)-Construction of building for the principal employer  at a new place is "work of that establishment"-Expression "work of an establishment "used in the definition of workmen" or "Contractor" is not the same as the expression "other work in any establishment" in S. 10-Workman need not be doing same as or incidental to that' of principal employer. Gammon India Ltd. v. Union of India, 1974 SCC (L & S) 252.

 **CHAPTER II**  
**THE ADVISORY BOARDS**  
        3**. Central Advisory Board**.-

(1) The Central Government shall, as soon as may be, constitute a board to be called the Central Advisory Contract Labour Board (hereinafter referred to as the Central Board) to advise the Central Government on such matters arising out of the administration of this Act as may be referred to it and to carry out other functions assigned to it under this Act.  
(2) The Central Board shall consist of--  
(a)     a Chairman to be appointed by the Central Government ;  
(b)     the Chief Labour Commissioner ( Central) , _ex officio_;  
(c)     such number of members, not exceeding seventeen but not less than eleven, as the Central Government may nominate to represent that Government, the Railways, the coal industry, the mining industry, the contractors, the workmen an any other interests which, in the opinion of the Central Government ought to be represented on the Central Board. ,  
(3) The number of persons to be appointed and members from each of the categories specified in sub-section (2), the term of office and other conditions of service of, the procedure to be followed in the  discharge of their functions by, and the manner of filling vacancies among, the members of the Central Board shall be such as may be prescribed :  
       Provided that the number of members nominated to represent the workmen shall not be less than the number of members nominated to represent the principal employers and the contractors.  
 [Top](#Section)  
 **NOTES**  
      Ss. 3 & 10-A member of the central board does not cease to be a member as soon as he ceases to represent the interest which he purports to represent on the board. _J.P. Gupta v; Union of India_, 1981 Lab IC 641 (Pat HC).  
       **4. State Advisory Board**.-

(1) The State Government may constitute a board to be called the State Advisory Contract-Labour Board (hereinafter referred to as the State Board) to advise the State Government on such matters arising. out of the administration of this Act as may be referred to it and to carry out other functions assigned to it under this Act.  
(2) The State Board shall consist of-  
 (a)     a Chairman to be appointed by the State Government ;  
 (b)     the Labour Commissioner, _ex officio_, or in his absence any other officer nominated by the State. Government in that behalf ;  
 (c)     such numbers, not exceeding eleven but not less than nine, as the State Government may nominate to represent that Government, the industry, the contractors, the workmen and any other interests which, in the opinion of the State Government, ought to be represented on the State Board.  
 (3) The number of persons to be appointed as members from each of the categories specified in sub-section (2), the term of office and other conditions of service of, the procedure to be followed in the discharge of their functions by, and the manner of filling vacancies among, the members of the State Board shall be such as may be prescribed:  
      Provided that the number of members nominated to represent the workmen shall not be less than the number of members nominated to represent the principal employers and the contractors.  
    

  **5\. Power to constitute committees**.-

      (1) The Central Board or the State Board, as the case may be, may constitute such committees and for such purpose or purposes as it may think fit.  
      (2) The committee constituted under sub-section (1) shall meet at such time and places and shall observe such rules of procedure in regard to the transaction of business at its meetings as may be prescribed.  
      (3) The members of a committee shall be paid such fees and allowances for attending its meetings as may be prescribed:  
       Provided that no fees shall be payable to a member who is an officer of Government or of any corporation established by any law for the time being in force.  
 [Top](#Section)  
**CHAPTER III**  
**REGISTRATION Of ESTABLISHMENTS EMPLOYING CONTRACT LABOUR**  
     

  **6. Appointment of registering officers**.--

The appropriate Government may, by an order notified in the Official Gazette-  
(a)     appoint such persons, being Gazetted Officers of Government, as it thinks fit to be registering officers for the purposes of this Chapter; and  
(b)     define the limits, within which a registering officer shall exercise the powers conferred on him by or under this Act.  
 **7\. Registration of certain establishment**.-

(1) Every principal employer of an establishment to which this Act applies shall, within such period as the appropriate Government may, by notification in the Official Gazette, fix in this behalf with respect to establishments generally or with respect to any class of them, make an application to the registering office: in the prescribed manner for registration of the establishment:  
 Provided that the registering officer may entertain any such application for registration after expiry of the period fixed in this behalf, if the registering officer is satisfied that the applicant was prevented by sufficient cause from making the application in time.  
(2) If the application for registration is complete in all respects, the registering officer shall register the establishment and issue to the principal employer of the establishment a certificate of registration containing such particulars as may be prescribed.   
**8\. Revocation of registration in certain cases**\--

If the registering officer is satisfied, either on a reference made to him in this behalf or otherwise, that the registration of any establishment has been obtained by mis-representation or suppression of any material fact, or- that for any other reason the registration has become useless or ineffective and, therefore requires to be revoked, the registering officer may, after giving an opportunity to the principal employer of the establishment to be heard and with the  previous approval of the appropriate Government, revoke the registration.  
 [Top](#Section)  
**9\. Effect of non-registration**.-

No principal employer of an establishment, to which this Act applies, shall-  
(a)     in the case of an establishment required to be registered under Section 7, but which has not been registered within the time fixed for the purpose under that section,  
(b)     in the case of an establishment the registration in respect of which has been revoked under Section 8,  employ contract labour in the establishment after the expiry of the period referred to in clause (a) or after the revocation of registration referred to in clause (b), as the case may be.  
       

**10\. Prohibition of employment of contract labour**.-

(1) Notwithstanding anything contained in this Act, the appropriate Government may, after consultation with the Central Board or, as the case may be, a State Board, prohibit, by notification in the Official Gazette, employment of contract labour in any process, operation or other work in any establishment.  
(2) Before issuing any notification under sub-section (1) in relation to an establishment, the appropriate Government shall have regard to the conditions of work and benefits provided for the contract labour that establishment and other relevant factors, such as-  
(a)     whether the process, operation or other work is incidental to, or necessary for the industry, trade, business, manufacture or occupation that is carried on in the establishment ;   
(b)     whether it is of perennial nature, that is to say, it is so of sufficient duration having regard to the nature of industry, trade, business, manufacture or occupation carried on in that establishment ;   
(c)     whether it is done ordinarily through regular workmen in that establishment or an establishment similar thereto;    
(d)     whether it is sufficient to employ considerable number of whole-time workmen.   
_Explanation._\- If a question arises whether any process or operation or other work is of perennial nature, the decision of the appropriate Government thereon shall be final.   
[Top](#Section)  
**NOTES**  
      **Criteria and circumstances for abolition of Contract Labour**.-_Feeding hoppers incidental and allied to main work-Loading and unloading sporadic and intermittent work._   
     The dispute related to the abolition of contract labour in the seeds godown and solvent extraction plants in the appellant's factory engaged in the manufacture of edible oils and its by products. The appellant company maintained that in both the departments the work was intermittent and sporadic type and hence contract labour was both efficient and economic. The union, on behalf of the workmen, challenged this standpoint and furnished charts, etc., to prove the continuous and perennial nature of the work. It also referred to the practice in certain other companies.   
    If the work for which contract labour is employed is incidental to and closely connected with the main activity of the industry and is of a perennial and permanent nature, the abolition of contract labour should be justified. It is also open to the Industrial Tribunal to have regard to the practice obtaining in other industries in or about the same area.   
     It is clear that the feeding of hoppers in the solvent extraction plant is an activity closely and intimately connected with the main activity of the appellant. This item of work is incidental to the nature of the industry carried on by the appellant, which must be done almost every day and there should be no difficulty in having regular workmen in the employment of the appellant to do this type of work. Also on comparison with other factories doing the same work it follows that the feeding of hoppers is an essential part of the industry carried on by the appellant and that it could very well be done by the departmental workman as is being done by others.   
     On merits the direction of the Industrial Tribunal abolishing contract labour regarding loading and unloading cannot be sustained. When it is shown that in similar establishment this type of work is not ordinarily done through regular workmen, but by contract labour that is a circumstance which will operate in favour of the appellant.    
     No doubt, the Industrial Tribunal referred to Section 10 of the Central Act, but the Tribunal misapplied those provisions when it directed abolition of contract labour regarding loading and unloading operations. Vegolis Pvt. Ltd. v. Workmen, (1971) 2 SCC 724, 730, 733,740.   
     Provincial Govt. has exclusive jurisdiction in regard to prohibition of employment of contract labour. Industrial Tribunal cannot issue directions to an establishment to abolish contract labour w.e.f. a date after coming into force of the Act. Vegoils Pvt. Ltd. v. Workmen, (1971) 2 SCC 724.   
      S.10 -Central Government is not required to put on record that they have examined the question of prohibition of contract labour in the establishments after taking into account each fact separately. It is for the person challenging the notification to establish that the notification in question had been issued on some extraneous considerations or without taking into account the relevant factors mentioned in S. 10(2). J._P. Gupta v. Union of India,_ 1981 Lab IC 641 (Pat HC).   
     S.10 (1)-A single notification prohibiting contract labour can be issued in respect or different establishments if the operation and nature of work are similar in all establishments Hence notification prohibiting/contract labour in coal mines was proper. J. _P. Gupta v. Union of India_, 1981 Lab IC 641 (Pat HC).   
[Top](#Section)  
**CHAPTER IV**  
**LICENSING OF CONTRACTORS**   
     

**11\. Appointment of licensing officers**.-

The appropriate Government may, by an order notified in the Official Gazette,-  
(a)     appoint such persons, being Gazetted Officers of Government, as  it thinks fit to be licensing officers for the purposes of this Chapter ; and   
(b)     define the limits, within which a licensing officer shall exercise the powers conferred on licensing officers by or under this Act.   
   

  **12. Licensing of contractors**.-

(1) With effect from such date as the appropriate Government may, by notification in the Official Gazette, appoint, no contractor to whom this Act applies, shall undertake or execute any work through contract labour except under and accordance with a licence issued in that behalf by the licensing officer.    
(2) Subject to the provisions of this Act, a licence under sub-section (1) may contain such conditions including, in particular, conditions as to hours or work, fixation of wages and other essential amenities in respect of contract labour as the appropriate Government may deem fit to impose in accordance with the rules, if any, made under Section 35 and shall be issued on payment of such fees and on the deposit of such sum, If any, as security for the due performance of the conditions as may be prescribed.   
**NOTES**  
S. 12-Sub-contractors or 'piece wagers' are equally responsible for obtaining licence and implementing the provisions of the Act and the Rules. Execution of a work in a government project by piece wagers through workers employed by them either directly or through khatedars must be in accordance with the licence obtained under S. 12 (1).  
Failure to obtain licence will amount to criminal offence punishable under Ss. 16 to 21 read with Rules 41 to 62 of the Rules. Labourers Working on Salal Hydro Project v. State of J & K, (1983) 2 SCC 181: (1983) 1 LLJ 494.   
       Ss. 12 & 2(1)(e)(ii)-Where a firm under all agreement undertook the work of holding and storage of another company's materials and for that purpose utilized the services of some labourers employed through sirdars, the firm, its partners and employees could not be prosecuted for not obtaining licence under S. 12 as the firm is an "establishment" within the meaning of S. 2(1)(e)(ii) and not the company's contractor. Assuming the partners and employees of the firm or any of them were principal employers, they could not be both contractors and principal employers in relation to the same establishment. Moreover, each of the sirdars was a contractor within the meaning of the act in relation to the firm i.e. the establishment. The concerned workmen having been supplied through the medium of sirdars, neither the firm nor the partners nor the employees could be deemed to be a contractor in relation to the said workmen. Their liability to take out a licence cannot, therefore, arise. _Feroze Sons v. B. C. Basu_, (1979) 54, FJR 158 (Cal).    
      S. 12 imposed a liability not to undertake or execute any work through contract labour without licence, a liability which continued until the licence was obtained and its requirement was complied with. It was an act which continued. Undertaking or executing any work through contract labour without a licence, therefore, constituted a fresh offence everyday on which it continued. _Padam Prasad Jain v. State of Bihar_, 1978 Lab IC 145.   
[Top](#Section)  
    

  **13. Grant of licences**.-

(1) Every application for the grant of a licence under sub-section (1) of Section 12 shall be made in the prescribed form and shall contain the particulars regarding the location of the establishment, the nature of process, operation or work for which contract labour is to be employed and such other particulars as may be prescribed.  

(2) The licensing officer may make such investigation in respect of the application received under sub-section (1) and in making any such investigation the licensing officer shall follow such procedure as may be prescribed.    
(3) A licence granted under this adapter shall be valid for the period specified therein and may be renewed from time to time for such period, and on payment of such fees and on such conditions as may be prescribed.  
    

   **14. Revocation, suspension and amendment of licences**.-

(1) If the licensing officer is satisfied, either on a reference made to him in this behalf or otherwise, that-  
(a)     a licence granted under Section 12 has been obtained by misrepresentation or suppression of any material fact, or    
(b)     the holder of a licence has, without reasonable cause, failed to comply with the conditions subject to which the licence has been granted or has contravened any of the provisions of this Act or the rules made thereunder, then without prejudice to any other penalty to which the holder of the licence may be liable under this Act, the licensing officer may after giving  the holder of the licence an opportunity of showing cause, revoke or suspend the licence or forfeit the sum, if any, or any portion thereof deposited as security for the due performance of the conditions subject to which the licence has been granted.    
(2) Subject to any rules that may be made in this behalf, the licensing, officer may vary or amend la licence granted under Section 12.  
[Top](#Section)  
**NOTES**  
Licensing Officer under S. 14 is not a Court. Provisions of this section does not violate Arts. 14 and 19 (1) (f). _Gammon India Ltd. v. Union of India,_ (1974) 1 SCC 596.   
    

  **15. Appeal**.-

(1) Any person aggrieved by an order made under Section 7, Section 8, Section 12 or Section 14 may, within thirty days from the date on which the order is communicated to him, prefer an appeal to an appellate officer who shall be a person nominated in this behalf by the appropriate Government:   
 Provided that the appellate officer may entertain the appeal after the expiry of the said period of thirty days, if he is satisfied that the appellant was prevented by sufficient cause from filing the appeal in time.  
(2) On receipt of an appeal under sub-section (1), the appellate officer shall, after giving the appellant an opportunity of being heard dispose of the appeal as expeditiously as possible.  
 [Top](#Section)  
 

**CHAPTER V**  
**WELFARE AND HEALTH OF CONTRACT LABOUR**  
   
       **16. Canteens**.-

(1) The appropriate Government may make rules requiring that in every establishment-  
(a)     to which this Act applies,  
(b)      wherein work requiring employment of contract labour is likely to continue for such period as may be prescribed, and  
(c)     wherein contract labour numbering one hundred or more is ordinarily employed by a contract, one or more canteens shall be provided and maintained by the contractor for the use of such contract labour.  
(2) Without prejudice to the generality of the foregoing power, such rules may provide for-  
(a)     the date by which the canteens shall be provided ;  
(b)     the number of canteens that shall be provided, and the standards in respect of construction, accommodation, furniture and other  equipment of the canteens; and  
(c)     the foodstuffs which may be served therein and the charges which may be made therefore.   
[Top](#Section)  
**NOTES**  
Provisions, held, are not unreasonable. _Gammon India Lid. v. Union of India,_ (1974) 1 SCC 596.   
       

**17\. Rest-rooms**.-

(1) In every place wherein contract labour is required to halt at night in connection with the work of an establishment-  
(a)     to which this Act applies, and  
(b)      in which work requiring employment of contract labour is likely  to continue for such period as may be prescribed, there shall be provided and maintained by the contractor for the use of the contract labour such number of rest-rooms or such other suitable alternative accommodation within such time as may be prescribed.   
(2) The rest-rooms or the alternative accommodation to be provided under sub-section (1) shall be sufficiently lighted and ventilated and shall be maintained in a clean and comfortable condition.   
[Top](#Section)  
**NOTES**  
Provisions, held are not unreasonable. Gammon India Ltd. v. Union of India, (1974) 1 SCC 596.  
   
 **18\. Other facilities**.-

It shall be the duty of every contractor employing contract labour in connection with the work of an establishment to which this Act applies, to provide and maintain-  
(a)     a sufficient supply of wholesome drinking water for the contract labour at convenient places;  
(b)     a sufficient number of latrines and urinals of the prescribed types so situated as to be convenient and accessible to the contract labour in the establishment; and  
(c)     washing facilities.   
[Top](#Section)  
**NOTES**  
Provision, held, reasonable_. Gammon India Ltd. v. Union of India_, (1974) 1 SCC 596.   
       

**19\. First-aid facilities**.-

There shall be provided and maintained by the contractor so as to be readily accessible during all working hours a first aid box equipped with the prescribed contents at every place where contract labour is employed by him.    
      

  **20. Liability of principal employer in certain cases**.-

(1) If any amenity required to be provided under Section 16, Section 17, Section 18 or Section 19 for the benefit of the contract labour employed in an establishment is not provided by the contractor within the time prescribed therefor, such amenity shall be provided by the principal employer within such time as may be prescribed.   
(2) All expenses incurred by the principal employer in providing the amenity may be recovered by the principal employer from the contractor either by deduction from any amount payable to the contractor under any contract or as a debt payable by the contractor.   
[Top](#Section)  
**NOTES**  
Ss. 20 & 21- Obligation to provide amenities conferred under the Act to the workers is on the principal employer. Government will be responsible for enforcement of those amenities where contractors engaged by it for executing its construction project fail to provide the amenities to its workers. Government's failure to perform its obligation amounts to violation of Art. 21 and workers can enforce their right by writ petition under Art. 32.    
         _Peoples Union for Democratic Rights v. Union of India_, (1982) 3 SCC 235: 1982 SCC (L & S) 275.             
      

  **21. Responsibility for payment of wages**.-

(1) A contractor shall be responsible for payment of wages to each worker employed by him as contract labour and such wages shall be paid before the expiry of such period as may be prescribed.    
(2) Every principal employer shall nominate a representative duly authorised by him to be present at the time of disbursement of wages by the contractor and it shall be the duty of such representative to certify the amounts paid as wages in such manner as may be prescribed.    
(3) It shall be the duty of the contractor to ensure the disbursement of wages in the presence of the authorised representative of the principal employer.   
(4) In case the contractor fails to make payment of wages within the prescribed period or makes short payment, then the principal employer shall be liable to make payment of wages in full or the unpaid balance due, as the case may be, to the contract labour employed by the contractor and recover the amount so paid from the contractor either by deduction from any amount payable to the contractor any contract as a debt payable by the contractor.   
[Top](#Section)  
**NOTES**  
        S. 21 -Payment of wages including overtime wages etc. must be made directly to the workers in full except with authorised statutory deductions, if any. Payment through khatedars after deducting any advance repayable by the workers to the khatedars or any messing charges etc. was not proper. Due amounts could be recovered from the workers after paying full wages.   
      _Labourers Working on Salal Hydro Project v.State of J. &. K.,_ (1983) 2 SCC 181: 1981 SCC (L & S) 289.   
 

**CHAPTER VI**  
**PENAL TIES AND PROCEDURE**  
 [Top](#Section)  
**22.Obstructions**.-

(1) Whoever obstructs an inspector in the discharge of his duties under this Act or refuses or wilfully neglects to afford the inspector any reasonable facility for making any inspection, examination, inquiry or investigation authorised by or under this Act in relation to an establishment to which, or a contractor to whom, this Act applies, shall be punishable with imprisonment for a term which may extend to three months, or with fine which may extend to five hundred rupees, or with both.   
(2) Whoever wilfully refuses to produce on the demand of an inspector any register or other document kept in pursuance of this Act or prevents or attempts to prevent or does anything which he has reason to believe is likely to prevent any person from appearing before or being examined by an inspector acting in pursuance of his duties under the Act, shall be punishable with imprisonment for a term which may extend to three months, or with fine which may extend to five hundred rupees, or with both.    
    

  **23. Contravention of provisions regarding employment contract labour**.-

Whoever contravenes any provision of this Act or of any rules made thereunder prohibiting, restricting or regulating the employment of contract labour, or contravenes any condition of a licence granted under this Act, shall be punishable with imprisonment for a term which may extend to three months, or with fine which may/extend to one thousand rupees, or with both, and in the case of a continuing contravention with an additional fine which may extend to one hundred rupees for every day during which such contravention continues after conviction for the first such  contravention.  
 [Top](#Section)  
**NOTES**  
       S. 23 -Mere allegation of contravention is not sufficient. The complainant has to allege as to who are those persons who have contravened the prohibition of or restriction on the employment of contract labour.  
_J.P. Gupta v. Union of India_, 1981 Lab IC 641 (Pat HC)   
       

**24\. Other offences**.-

If any person contravenes any of the provisions of this Act or of any rules made thereunder for which no other penalty is elsewhere provided, he shall be punishable with imprisonment for a term which may extend to three months, or with fine which may extend to one  thousand rupees, or with both.  
   
      

   **25. Offences by companies**.-

(1) If the person committing an offence under this Act is a company, the company as well as every person in charge of, and responsible to, the company for the conduct of its business at the  time of commission of the offence shall be deemed to be guilty of the offence and shall be liable to be proceeded against and punished accordingly:   
 Provided that nothing contained in this sub-section shall render any such person liable to any punishment if he proves that the offence was committed without his knowledge or that he exercised all due diligence to prevent the commission of such offence.   
 (2) Notwithstanding anything contained in sub-section (1), where an offence under this Act has been committed by a company and it is proved that the offence has been committed with the consent or connivance of, Or that the commission of' the offence is attributable to any neglect on the part of any director, manager, managing agent or any other officer of the company, such director, manager, managing agent or such other officer shall also be deemed to be guilty of that offence and shall be liable to be proceeded against and punished accordingly.   
       _Explanation_.-For the purpose of this section-  
(a)     "company" means any body corporate and includes a firm or other association of individuals; and   
(b)     "director", in relation to a firm means a partner in the firm.   
[Top](#Section)  
      

  **26. Cognizance or offences**.-

No court shall take cognizance of any offence under this Act except on a complaint made by, or with the previous sanction in writing of, the inspector and no court inferior to that of a Presidency Magistrate or a magistrate of the first class shall try any offence punishable under this Act.    
    

   **27. Limitation or prosecution**.-No court shall take cognizance of an offence punishable under this Act unless the complaint thereof is made within three months from the date on which the alleged commission of the offence came to the knowledge of an inspector:   
       Provided that where the offence consists of disobeying a written order made by an inspector, complaint thereof may be made within six months of the date on which the offence is alleged to have been committed.

 **CHAPTER VII**  
**MISCELLANEOUS**  
 [Top](#Section)  
       

  **28. Inspecting staff**.-

(1) The appropriate Government may, by notification in the Official Gazette, appoint such persons as it thinks fit to be inspectors for the purposes of this Act, and define the local limits within which they shall exercise their powers under this Act.   
(2) Subject to any rules made in this behalf, an inspector may, within the local limits for which he is appointed-  
(a)     enter, at all reasonable hours, with such assistance (if any), being persons in the service of the Government or any local or other public authority as he thinks fit, any premises or place where contract labour is employed, for the purpose of examining any register or record or notices required to be kept or exhibited by or under this. Act or rules made 'thereunder, and require the production thereof for inspection ;   
(b)     examine any person whom he finds in any such premises or place and who, he has reasonable cause to believe, is a workman employed therein ;   
(c)     require any person giving out work and any workman, to give any information, which is in his power to give with respect to the names and addresses of the persons to, for and from whom the work is given out or received, and with respect to the payments to be made for the work ;   
(d)     seize to take copies of such register, record of wages or notices or portions ,thereof as he may consider relevant in respect of an offence under this Act which he has reason to believe has been committed by the principal employer or contractor; and   
(e)     exercise such other powers as may be prescribed.   
(3) Any person required to produce any document or thing or to give any information required by an inspector under sub-section (2) shall be deemed to be legally bound to do so within the meaning of Section 175 and Section 176 of the Indian Penal Code, 1860 (45 of 1860).   
(4) The provisions of the Code of Criminal Procedure, 1898 (5 of 1898), shall, so far as may be, apply to any search or seizure under sub-section (2)as they apply to any search or seizure made under the authority of a warrant issued under Section 98 of the said Code\* .   
[Top](#Section)  
**NOTES**   
S. 28-Frequent, detailed and thorough inspection should be undertaken by senior officers for ensuring compliance with S. 21. Labourers Working Salal Hydro Project v. State of J. & K., (1983) 2SCC 181.   
    

  **29. Registers and other records to be maintained**.-

(1) Every principal employer and every contractor shall maintain such registers and records giving such particulars of contract labour employed, the nature of work performed by the contract labour, the rates of wages paid to the contract labour and such other particulars in such form as may be prescribed.   
(2) Every principal employer and every contractor shall keep exhibited in such manner as may be prescribed within the premises of the establishment where the contract labour is employed, notices in the prescribed form containing particulars about the hours of work, nature of duty and such other information as may be prescribed.   
    

**30\. Effect of laws and agreements inconsistent with this Act.-**

(1) The provisions of this Act shall have effect notwithstanding anything inconsistent therewith contained in any other law or in the terms of any agreement or contract of service, or in any standing orders applicable to the establishment whether made before or after the commencement of the Act:   
Provided that where under any such agreement, contract of service standing orders the contract labour employed in the establishment are entitled to benefits in respect of any matter which are more favourable to them than those to which they would be entitled under this Act the contract the labour shall continue to be entitled to the more favourable benefits in respect of that matter, notwithstanding that they received benefits in respect of other matters under this Act.    
(2) Nothing contained in this Act shall be construed as precluding any such contract labour from entering into an agreement with the principal employer or the contractor, as the case may be, for granting them rights or privileges in respect of any matter which are more favourable to them than those to which they would be entitled under this Act.   
 

**31\. Power to exempt in special cases**.-

The appropriate Government may, in the case of an emergency, direct, by notification in the Official Gazette, that subject to such conditions and restrictions if any, and for such period. or periods as may be specified in the notification, all or any of the  provisions of this Act or the rules made thereunder shall not apply to any establishment or class of establishments or any class of contractors.    
 

**32\. Protection of action taken under this Act**.-

(1) No suit, prosecution or other legal proceedings shall lie against any registering officer, licensing officer or any other Government servant or against any member of the Central Board or the State Board as the case may be, for anything which is in good faith done or intended to be done in pursuance of this Act or any rule or order made thereunder.   
(2) No suit or other legal proceeding shall lie against the Government for any damage caused or likely to be caused by anything which is in good faith done or intended to be done in pursuance of this Act or any rule or order made thereunder   
       

   **33. Power to give directions**.-

The Central Government may give directions to the Government of any State as to the carrying into execution in the State of the provisions contained in this Act.   
      

  **34. Power to remove difficulties**.-

If any difficulty arises in giving effect to the provisions of this Act, the Central Government may, by order published in the Official Gazette, make such provisions not inconsistent  with the provisions of this Act, as appears to it to be necessary or expedient for removing the difficulty.   
[Top](#Section)  
**NOTES**  
       Held, does not suffer from vice of excessive delegation. Gammon of India Ltd. v. Union of India, (1974) 1 SCC 596.   
       

**35\. Power to make rules**.-

(1) The appropriate Government may, subject to the condition of previous publication, make rules for carrying out the purposes of this Act.   
(2) In particular, and without prejudice to the generality of the fore going power, such rules may provide for all or any of the following matters, namely-   
(a) The number of persons to be appointed as members representing various interests on the Central Board and the State Board, the term of their office and other conditions of service, the procedure to be followed in the discharge of their functions and the manner of filling vacancies ;   
(b) The times and places of the meetings of any committee constituted under this Act, the procedure to be followed at such meetings  including the quorum necessary for the transaction of business, and the. fees and allowances that may be paid to the members of  a committee ;   
(c) The manner in which establishments may be registered under Section 7, the levy of a fee therefor and the form of certificate of registration ;   
[Top](#Section)  
**NOTES**  
Levy of fee by Central and State Govts. for registration, licence and renewal of licence does not amount to levy of tax. _Gammon India Ltd. v. Union of India_, (1974) 1 SCC 596: 1974 SCC (L & S) 252.   
(d) The form of application for the grant or renewal of a licence under Section 13 and t4e particulars it may contain   
(e) The manner in which an investigation is to be made in respect of  an application for the grant of a licence and the matters to be  taken into account in granting or refusing a licence   
(f) The form of a licence which may be granted or renewed under  Section 12 and the conditions subject to which the licence may be granted or renewed, the fees to be leveled for the grant or renewal of a licence and the deposit of any sum as security for the performance of such conditions   
(g) The circumstances under which licences may be varied or amended under Section 14   
(h) The form and manner in which appeals may be filed under Section 15 and the procedure to be followed by appellate officers in disposing of the appeals   
(i) The time within which facilities required by this Act to be provided and maintained may be so provided by the contractor and in case of default on the part of the contractor, by the principal employer;   
(j) The number and types of canteens, rest-rooms, latrines and urinals   that should be provided and maintained   
(k) The type of equipment that should be provided in the first aid boxes  
(l) The period within which wages payable to contract labour should be paid by the contractor under sub-section (1) of Section 21  
(m) The form of registers and records to be maintained by principal employers and contractors  
(n) The submission of returns, forms in which, and the authorities to which, such returns may be submitted  
(o) The collection of any information or statistics in relation to contract labour; and   
(p) Any other matter which has to be, or may be, prescribed under this Act.   
(3) Every rule made by the Central Government under this Act shall be laid as soon as may be after it is made, before each House of Parliament while it is in session for a total period of thirty days which may be comprised is one session or in two successive sessions, and if before the expiry of the session in which it is so laid or the session immediately following, both Houses agree in making any modification in the rule or both Houses be of no effect as the case may be; so, however, that any such modification or annulment shall be without prejudice to the validity of anything previously done under that rule.  
 [Top](#Section)

* * *

**_THE CONTRACT LABOUR (REGULATION AND ABOLITION) CENTRAL RULES, 1971_**  
 **CHAPTER I**   
**1\. Short tide and commencement**.-

(1) These rules may be called the Contract Labour (Regulation and Abolition) Central Rules, 1971.   
(2) They shall come into force on the date of their publication1 in the Official _Gazette._   
**2\. Definitions**. -In these rules, unless the subject or context otherwise requires:  
(a)     "Act" means the Contract Labour (Regulation and Abolition) Act, 1970 ;   
(b)     "Appellate Officer" means the Appellate Officer appointed by the Central Government under sub-section (1) of Section 15 ;   
(c)      "Board" means the Central Advisory Contract Labour Board constituted under Section 3 ;   
(d)      "Chairman" means the Chairman of the Board;    
(e)      "Committee" means a Committee constituted under sub-section (1) of Section 5 ;   
(f)       "Form" means a form appended to these rules;   
(g)     "Section" means a section of the Act.   
 [Top](#Section)  
**CHAPTER II**  
**CENTRAL BOARD**   
3\. The Board shall consist of the following members :  
(a)     a Chairman to be appointed by the Central Government ;   
(b)     the Chief Labour Commissioner (Central)-_ex officio_;   
(c)     one person representing the Central Government, to be, appointed by that Government from amongst its officials;    
(d)     \[two\] persons representing the Railways, to be appointed by Central Government after consultation with the Railway Board ;   
(e)     \[five\] persons, one representing the employers in coal mines, \[two\] representing the employers in other mines and two representing contractors to whom the Act applies, to be appointed by the Central Government after consultation with such organisations, if any, of the employers and the contractors as may be recognised by the Central Government ;   
(f)       \[seven\] persons,\[two\] representing the employees in the Railways, one representing the employees in coal mines, \[two\] representing the employees in other mines, and two representing the employees of the contractors to whom the Act applies, to be appointed by the Central Government after consultation with such organisations, if any, of employees representing the respective interest as may be recognised by the Central Government.  
 [Top](#Section)

1.

Published in Gazette of India, Extra., Part II, Section 3 (i), dated 10th February, 1971, vide Noti. No. GSR 191, dated February 1, 1971, pp. 173-211.

2.

Subs. by Noti. No. GSR 1643, dated, 16-10-1971 (w.e.f. 30-10-1971).

3.

Subs. by Noti. No. GSR 598, dated 23-5-1973 (w.e.f. 2-6-1973).

   
      

**4\. Terms of office**.-

 (1) The Chairman of the Board shall hold office as such for a period of three years from the date on which his appointment is first notified in the Official _Gazette._   
 (2) Each of the members of the Board, referred to in clauses (c) and (d) of Rule 3, shall hold office as such during the pleasure of the President.   
 (3) Each of the members referred to in clauses (e) and (f) of Rule 3 shall hold office as such for a period of three years commencing from the date on which his appointment is first notified in the Official _Gazette :_   
 Provided that where the successor of any such member has not been notified in the Official Gazette on or before the expiry of the said period of three years, such member shall, notwithstanding the expiry of the period of his office, continue to hold such office until the appointment of his successor has been notified in the Official Gazette.   
 (4) If a member is unable to attend a meeting of the Board, the Central Government or the body which appointed or nominated him may, by notice in writing signed on its behalf and by such member and addressed to the Chairman of the said Board, nominate a substitute in his place to attend the meeting and such a substitute member shall have all the rights of a member in respect of that meeting and any decision taken at the meeting shall be binding on the said body.   
[Top](#Section)  
         

**5\. Resignation**.-

(1) A member of the Board, not being an _ex officio_ member, may resign his office by a letter in writing addressed to the Central Government.   
(2) The office of such a member shall fall vacant from the date on which his resignation is accepted by the Central Government, or on the expiry of thirty days from the date of receipt of the letter of resignation by that Government whichever is earlier.\]  
      

**6\. Cessation of membership**.-

If any member of the Board, not being an ex officio member , fails to attend three consecutive meetings of the Board, without obtaining the leave of the Chairman for such absence, he shall cease to be a member of the Board :   
       Provided that the Central Government may, if it is satisfied that such member was prevented by sufficient cause from attending three consecutive meetings of the Board., direct that such cessation shall not take place and on such direction being made, such member shall continue to be a member of the Board.  
       

**7\. Disqualification for membership**.-

(1) A person shall be disqualified for being reappointed, and for being a member of the Board :-  
(i)  if he is of unsound mind and stands so declared by a competent Court; or   
(ii) if he is an undercharged insolvent; or   
(iii)if he has been or is convicted of an offence which, in the opinion of the Central Government, involves moral turpitude ;  
 [Top](#Section)

4.

Subs. by Noti. No. GSR 520, dated 10-5-1974 (w.e.f. 25-5-1974).

(2) If a question arises as to whether a disqualification has been incurred under sub-rule (1), the Central Government shall decide the same.   
    

**8\. Removal from membership**.-

The Central Government may remove from office any member of the Board, if in its opinion such a member has ceased to represent the interest which he purports to represent on the Board:   
Provided that no such member shall be removed unless a reasonable opportunity is given to him of making any representation against the proposed action.   
    

**9\. Vacancy-**

When a vacancy occurs or is likely to occur in the, membership of the Board the Chairman shall submit a report to the Central Government and on receipt of such report the Central Government shall take steps to fill the vacancy by making an appointment from amongst the category of persons to which the person vacating membership belonged and the person so appointed shall hold office for the remainder of the term of office of the member in whose place he is appointed.  
      

**10\. Staff.**\-

(1) (i) The Central Government may appoint one of its officials as Secretary to the Board and appoint such other staff as it may think necessary to enable the Board to carry out its functions.   
    (ii) The salaries and allowances payable to the staff and the other conditions of service of such staff shall be such as may be decided by the Central Government.   
The Secretary-  
(i)shall assist the Chairman in convening meetings of the Board ;   
(ii)may attend the meetings but shall not be entitled to vote at such meetings ;   
(iii)shall keep a record of the minutes of such meetings; and   
(iv)shall take necessary measure to carry out the decisions taken at meetings of the Board.   
     

**11\. Allowances of members**.-

(1) The travelling allowance of an official member shall be governed by the rules applicable to him for journey performed by him on official duties and shall be paid by the authority paying his salary.    
(2) The non-official members of the Board shall be paid travelling allowance for attending the meeting of the Board at such rates as are admissible to Grade I Officers of the Central Government and daily allowances shall be calculated at the maximum rate admissible to Grade I Officers of the Central Government in their respective places.   
    

**12\. Disposal of business**.-

Every question which the Board is required to take into consideration shall be considered at a meeting, or, if the Chairman so directs, by sending the necessary papers to every member for opinion, and the question shall be disposed of in accordance with the decision of the majority:    
     Provided that in the case of equality of votes, the Chairman shall have a second or a casting vote.   
    _Explanation._\-"Chairman for the purposes of this rule shall include the Chairman nominated under Rule 13 to preside over a meeting.   
[Top](#Section)  
    

**13\. Meetings**.-

(1) The Board shall meet at such places and times as may be specified by the Chairman.   
(2) The Chairman shall preside over every meeting of the Board at which he is present and in his absence nominate a member of the Board to preside over such meeting.   
    

**14\. Notice of meetings and 1ist of business**.-

(1) Ordinarily seven days notice shall be given to the members of a proposed meeting.   
(2) No business which is not on the list of business for a meeting shall be considered at that meeting without the permission of the Chairman.   
     

**15\. Quorum**.-No business shall be transacted at any meeting unless least five members are present :   
     Provided that if at any meeting less than five members are present, the chairman may adjourn the meeting to another date informing members present and giving notice to the other members that he proposes to dispose the business at the adjourned meeting whether there is prescribed quorum not, and it shall thereupon be lawful for him to dispose of the business at adjourned meeting irrespective of the number of members attending.   
     

**16\. Committee of the Board.-**

(1)  (i) The Board may constitute such Committees and for such purpose or purposes as it may think fit.   
      (ii) While constituting the Committee the Board may nominate one of members to be the Chairman of the Committee.   
5(2) (i) The Committee shall meet at such times and places as the chairman of the said Committee may decide.   
      (ii) The provisions of Rules 12, 13(2), 14 and 15 shall apply to the committee for transaction of business at its meetings as they apply to the Board, subject to the modification that the quorum specified in Rule 15 shall be 'one-third of the members' instead of 'five members'.\]   
  (3) The provisions of Rule 11 shall apply to the members of the Committee for attending the meetings of the Committee, as they apply to the members of the Board.  
 [Top](#Section)

**CHAPTER III**  
**REGISTRATION AND LICENSING**   
 

**17\. Manner of making application for registration of establishments.**\-

(1) The application referred to in sub-section (1) of Section 7 shall be made in triplicate, in Form I to the registering officer of the area in which the establishment sought to be registered is located.   
(2) The application referred to in sub-rule (1) shall be accompanied by treasury receipt showing payment of the fees for .the registration of the establishment.   
(3) Every application referred to in sub-rule (1) shall be either personally delivered to the registering officer or sent to him by registered post. 

(4) On receipt of the application referred to in sub-rule (1), the registering officer shall, after noting thereon the date of receipt by him of the application, grant an acknowledgment to the applicant.     

**18\. Grant of certificate of registration**.-

(1) The certificate of registration granted under sub-section (2) of Section 7 shall be in Form II.   
(2) Every certificate of registration granted under sub-section (2) of Section 7 shall contain the following particulars, namely-  
(a)     the name and address of the establishment ;   
(b)     the maximum number of workmen to be employed as contract labour in the establishment ;   
(c)     the type of business, trade, industry, manufacture or occupation  which is carried on in the establishment;    
(d)     such other particulars as may be relevant to the employment of contract labour in the establishment.   
(3) The registering officer shall maintain a register in Form III showing the particulars of establishments in relation to which certificates of registration have been issued by him.  
(4) If, in relation to an establishment, there is any change, in the particulars specified in the certificate of registration, the principal employer of the establishment shall intimate to the registering officer, within. Thirty days from the date when such change takes place, the particulars of, and the reasons for, such change.   
     

**19\. Circumstances in which application for registration may be rejected**.-

(1) If any application for registration is not complete in all respects, the registering officer shall require the principal employer to amend the application so as to make it complete in all respects.  
(2) If the principal employer, on being required by the registering officer to amend his application for registration, omits or fails to do so, the registering officer shall reject the application for registration.   
     

**20\. Amendment of certificate of registration**.-

(1) Where on receipt of the intimation under sub-rule (4) of Rule 18, the registering officer is satisfied that an amount higher than the amount, which has been paid by the principal employer as fees for the registration of the establishment is payable, he shall require such principal employer to deposit a sum which, together with the amount already paid by such principal employer, would be equal to such higher amount of fees payable for the registration of the establishment and to produce the treasury receipt showing such deposit.   
(2) Where, on receipt of the intimation referred to in sub-rule (4) of Rule 18, the registering officer is satisfied that there has occurred a charge in the particulars of the establishment, as entered in the register in Form III, he shall amend the said register and record therein the change which has occurred :   
Provided that no such amendment shall affect anything done or any action taken or any right, obligation or liability acquired or incurred before such amendment:   
      Provided further that the registering officer shall not carry out any amendment in the register in Form III unless the appropriate fees have been deposited by the principal employer. 

**21\. Application for a licence.-**

**(**1) Every application by a contractor for the grant of a licence shall be made in triplicate, in Form IV, to the licensing officer of the area in which the establishment, in relation to which he is the contractor, is located.   
(2) Every application for the grant of a licence shall be accompanied by a certificate by the principal employer in Form V to the effect that the applicant has been employed by him as a contractor in relation to. His establishment and that he undertakes to be bound by all the provisions of the Act and the rules made thereunder insofar as the provisions are applicable to him as principal employer in respect of the employment of contract labour by the applicant.   
(3) Every such application shall be either personally delivered to the licensing officer or sent to him by registered post.   
(4) On receipt of the application referred to in sub-rule (1), the licensing officer shall, after noting thereon the date of receipt of the application, grant an acknowledgment to the applicant.   
(5) Every application referred to in sub-rule (1) shall also be accompanied by a treasury receipt showing-  
(i)  the deposit of the security at the rates specified in Rule 24, and   
(ii) the payment of the fees at the rates specified in Rule 26.   
     **22. Matters to be taken into account in granting or refusing a licence**.-In granting or refusing to grant a licence, the licensing officer shall take the following matters into account, namely-  
(a)     whether the applicant-  
(i) is a minor, or   
(ii) is of unsound mind and stands so declared by a competent court, or   
(iii)is an undischargedsss insolvent, or   
(iv) has been convicted (at any time during a period of five years immediately preceding the date of application) of an Offence which, in the opinion of the Central Government, involves moral turpitude; -  
(b) whether there is an order of the appropriate Government or an award or settlement for the abolition of contract labour in respect of the particular type of work in the establishment for which the applicant is a contractor ;   
(c) whether any order has been made in respect of the applicant under sub-section (1) of Section 14, and, if so, whether a period of three years has elapsed from the date of that order ;   
(d) whether the fees for the application have been deposited at the rates specified in Rule 26; and   
(e) whether security has been deposited by the applicant at the rates specified in Rule 24.  
  [Top](#Section)  
      

**23\. Refusal to grant licence**.-

6\[(1) On receipt of the application from the contractor, and as soon as possible thereafter, the Licensing Officer shall investigate or cause investigation to be made to satisfy himself about the correctness of the facts and particulars furnished in such application and the eligibility to the applicant for a licence.\]   
 (2) (i) Where the licensing officer is of opinion that the licence should not be granted, he shall, after affording reasonable opportunity to the applicant to be heard, make an order rejecting the application.   
     (ii) The order shall record the reasons for the refusal and shall be communicated to the applicant.   
    

**24\. Security**.-

      (1) Before a licence is issued, an amount calculated at the rate of Rs. 30 for each of the workmen to be employed as contract labour, in respect of which the application for licence has been made, shall be deposited by         the contractor for due performance of the conditions of the licence and compliance with the provisions of the Act or the rules made thereunder:   
       Provided that where the contractor is a Co-operative Society, the amount deposited as security shall be at the rate of Rs. 5 for each workman to be employed as a contract labour.  
        (1A) Where the applicant for the licence was holding a licence in regard to another work and that licence had expired, the licensing officer, if he is of the view that any amount out of the security deposited in respect of          that licence is to be directed to be refund to the applicant under Rule 31, may, on an application made for that purpose in Form VA by the applicant adjust the amount so to be refunded towards the security required to          be deposited in respect of the application for the new licence and the applicant need deposit, in such a case, only the balance amount, if any, after making such adjustment.  
     (2) \[The amount of security, or the balance amount, required to be deposited under sub-rule (1) or, as the case may be, under sub-rule (1A)\] shall be paid in the local treasury under the Head of Account "Section T-              Deposits and Advances-Part II Deposits not bearing interest-(c) Other Deposit Accounts-Departmental and Judicial Deposits-Civil Deposits, under Contract Labour (Regulation and Abolition) Act, 1970 (Central)".   
**NOTES**  
     The rate of Rs. 30 per workman as security money does not offend Art. 14. _Gammon India ltd. v. Union of India_, (1974) 1 SCC 596.  
   
    

**25**. **Forms and terms and conditions of licence**.-

(1) Every licence granted under sub-section (1) of Section 12 shall be in Form VI.   
(2) Every licence granted under sub-rule (1) or renewed under Rule 29 shall be subject to the following conditions, namely-  
(i) the licence shall be non-transferable ;   
(ii)the number of workmen employed as contract labour in the establishment shall not, on any day, exceed the maximum number specified in the licence ;

(iii)save as provided in these rules, the fees paid for the grant, or as the case may be, for renewal of the licence shall be non-refundable;    
(iv)the rates of wages payable to the workmen by the contractor shall not be less than the rates prescribed under the Minimum Wages Act, 1948 (11 of 1948), for such employment where applicable, and where the rates have been fixed by agreement, settlement or award, not less than the rates so fixed ;   
(v)(a) in cases where the workmen employed by the contractor perform the same or similar kind of work as the workmen directly employed by the principal employer of the establishment, the wage rates, holidays, hours of work and other condition of service of the workmen of the contractor shall be the same as applicable to the workmen directly employed by the principal employer of the establishment on the same or similar kind of work:   
**Provided that in the case of any disagreement with regard to the type of work the same shall be decided by the Chief Labour Commissioner (Central) \[\*\*\*\]9 ;**  
(b) in other cases the wage rates holidays, hours of work and conditions of service of the workmen of the contractor shall be such as may be specified in this behalf by the Chief Labour Commissioner (Central);   
_Explanation._\-While determining the wage rates, holidays, hours of work and other conditions of service under (b) above, the Chief Labour Commissioner shall have due regard to the wage rates, holidays, hours of work and other conditions of service obtaining in similar employments;   
**NOTES**  
      R. 25 (2) (v) (b)-Held, reasonable and valid. Gammon India Led. v. Union of India. 1974) 1 SCC 596.  
(vi) (a) in every establishment where twenty or more Women are ordinarily employed as contract labour, there shall be provided two rooms of reasonable dimensions for the use of their children under the age of six years;   
(b) one of such rooms shall be used as a play room for the children and the other as bed room for the children;   
(c) the contractor shall supply adequate number of toys and games in the play room and sufficient number of cots and beddings in the sleeping room;   
(d) the standard of construction and maintenance of the crches shall be such as may be specified in this behalf by the Chief Labour Commissioner (Central);   
(vii) the licence shall notify any change in the number of workmen or the conditions of work to the licensing officer;   
(viii) the licensee shall, within fifteen days of the Commencement and completion of each contract work submit a return to the Inspector, appointed under Section 28 of the Act, intimating the actual date 

of the commencement or, as the case may be, completion or such contract work in Form VI-A\];   
(ix) no female contract labour shall be employed by any contractor before 6.00 a.m. or after 7 .00 p.m. Provided that this clause shall not apply to the employment of women in pithead baths, crches and canteens and as to midwives and nurses in hospitals and dispensaries.  
      

**26, Fees**.-

(1) The fees to be paid for the grant of a certificate of registration under Section 7 shall be as specified below, namely:    
 if the number of workmen proposed to be employed on contract on any day-  
            Rs. P.  
(a) is 20                                                                                                                      20.00             
(b) exceeds 20 but does not exceed 50                                                                 50.00  
(c) exceeds 50 but does not exceed l00                                                                100.00  
(d) exceeds l00 but does not exceed 200                                                             200.00  
(e) exceeds 200 but does not exceed 400                                                            400.00  
(f) exceeds 400                                                                                                          500.00  
   
       (2) The fees to be paid for the grant of a licence Under Section 12 shall be as specified below:  
If the number of workmen employed by the contractor on any day-  
  Rs. P.  
(a) is 20,                                                                                                              5.00  
(b) exceeds 20 but does not exceed 50                                                        12.50  
(c) exceeds 50 but does not exceed l00                                                         25.00  
(d) exceeds l00 but does not exceed 200                                                       50.00              
(e) exceeds 200 but does not exceed 400                                                    100.00             
(i) exceeds 400                                                                                                1500.00  
   
     

**27\. Validity of the licence**,-

Every licence granted under Rule 25 or renewed under Rule 29 shall remain in force for twelve months from the date it is granted or renewed.   
    

**28\. Amedment of the licence**.-

(1) A licence issued under Rule 25 or renewed under Rule 29 may, for good and sufficient reasons, be amended by the licensing officer.   
(2) The contractor who desires to have the licence amended shall submit to the licensing officer an application stating the nature of the amendment and reason therefor.   
(3) (i) If the licensing officer allows the application he shall require the applicant to furnish a treasury receipt for the amount, if any, by which the fees that would have been payable if the licence had been originally issued            in the amended form exceed the fees originally paid for the licence.  
    (ii) On the applicant furnishing the requisite treasury receipt the licence shall be amended according to the orders of the licensing office

(4) Where the application for amendment is refused, the licensing officer shall record the reasons for such refusal and communicate the same to the applicant.   
     

**29\. Renewal of licence**.,,-

(1) Every contractor shall apply to the licensing officer for renewal of the licence.  
(2) Every such application shall be in Form VII in triplicate and shall be made not less than thirty days before the date on which the licence expires, and if the application is so made, the licence shall be deemed to have been renewed until such date when the renewed licence is issued.   
(3) The fees chargeable for renewal of the licence shall be the same as for the grant thereof :   
     Provided that if the application for renewal is not received within the time specified in sub-rule (2), a fee of 25 per cent in excess of the fee ordinarily payable for the licence shall be payable for such renewal;   
     Provided further that in case where the licensing officer is satisfied that the delay in submission of the application is due to unavoidable circumstances beyond the control of the contractor, he may reduce or remit as he thinks fit the payment of such excess fee.   
   

**30**. **Issue of duplicate certificate of registration or licence**.-

Where a certificate of registration or a licence granted or renewed under the preceding rules has been lost, defaced or accidentally destroyed, a duplicate may be granted on payment of fees of rupees five.   
   

**31\. Refund of security**.-

(1) (i) On the expiry of the period of licence the contractor may, if he does not intend to have his licence renewed, make an application to the Licensing Officer for the refund of the security deposited by him under Rule 24.   
     (ii) If the Licensing Officer is satisfied that there is no breach of the conditions of licence or there is no order under Section 14 for the forfeiture of security or any portion thereof, he shall direct the refund of the security to      the applicant.    
(2) If there is any order directing the forfeiture of any portion of the security, the amount to be forfeited shall be deducted from the security deposit, and balance, if any, refunded to the applicant.   
(3) Any application for refund shall, as far as possible, be disposed of within 60 days of the receipt of the application.   
    

**32\. Grant of temporary certificate of registration and licence**.-

(1) Where conditions arise in an establishment requiring the employment of contract labour immediately and such employment is estimated to last for not more than fifteen days, the principal employer of the establishment or the contractor, as the case may be, may apply for the temporary certificate of registration or licence to the registering officer, or the licensing officer, as the case may be, having jurisdiction over the area in which the establishment is situated.   
(2) The application for such temporary certificate of registration or licence shall be made in triplicate in Forms VIII and X respectively and shall be accompanied by a treasury receipt **12**\[\*  \* \*\] drawn in favour of the appropriate registering or licensing officer, as the case may be, showing the payment of appropriate fees and in the case of licence the appropriate amount of security also.   
(3) On receipt of the application, complete in all respects, and on being satisfied either on affidavit by the applicant or otherwise that the work in respect of which the application has been made would be finished in a period of fifteen days and was of a nature which could not but be carried out immediately, the registering officer or the licensing officer, as the case may be, shall forthwith grant a certificate of registration in Form IX or a licence in Form XI, as the case may be, for a period of not more than fifteen days.   
(4) Where a certificate of registration or licence is not granted the reasons therefor shall be recorded by the registering officer or the licensing officer, as the case may be.   
(5) On the expiry of the validity of the registration certificate the establishment shall cease to employ in the establishment contract labour in respect of which the certificate was given.  
(6) The fees to be paid for the grant of the certificate of registration under sub-rule (3) shall he as specified below:  
If the number of workmen proposed to be employed on the contract on any day-  
Rs. P.  
(a) exceeds 20 but does not exceed 50                                                                10. 00  
(b) exceeds 50 but does hot exceed 200                                                              20. 00  
(c) exceeds 200                                                                                                 30. 00  
 [Top](#Section)  
    (7) The fees to be paid for the grant of a licence under sub-rule (3) shall be as specified below :-  
If the number of workmen to be employed by the contractor on any day-  
Rs. P.  
(a) exceeds 20 but does not exceed 50                                                               5. 00  
(b) exceeds 50 hut does not exceed 200                                                             20. 00  
(c) exceeds 200                                                                                                30. 00  
   
(8) The provision of Rule 23 and Rule 24 shall apply to the refusal to grant licence or to grant licence under sub-rule (4) and sub-rule (3) respectively.   
 

**CHAPTER IV**  
**APPEALS AND PROCEDURE**   
**33.** (1) (i) Every appeal under sub-section (1) of Section 15 shall be preferred in the form of a memorandum signed by the appellant or his authorised agent and presented to the Appellate Officer in person or sent to him by registered post.  
 [Top](#Section)

12.

_Omitted_ by Noti. No. GSR 870, dated 26-7-1974 (w.e.f. 10-8-1974).

   
 (ii) The memorandum shall be accompanied by a certified copy of the order appealed from and a treasury receipt for Rs. 10.   
    (2) The memorandum shall set forth concisely and under distinct heads the grounds of appeal to the order appealed from.   
   

**34**

(1) Where the memorandum of appeal does not comply with the provisions of sub-rule (2) of Rule 33 it may be rejected or returned to appellant for the purpose of being amended within a time to be fixed by the Appellate Officer.   
(2) Where the Appellate Officer rejects the memorandum under sub- rule (1) he shall record the reason for such rejection and communicate the order to the appellant.   
(3) Where the memorandum of appeal is in order the Appellate Officer shall admit the appeal, endorse thereon the date of presentation and shall register the appeal in a book to be kept for the purpose called the Register of Appeals.   
(4) (i) When the appeal been admitted, the Appellate Officer shall send the notice of the appeal to the Registering Officer or the Licensing Officer as the case may be from whose order the appeal has been preferred and the        Registering Officer or the Licensing Officer shall send the record of the case to the Appellate Officer.   
    (ii) On receipt of the record, the Appellate Officer shall send a notice to the appellant to appear before him at such date and time as may be specified in the notice for the hearing of the appeal.   
    

**35\. Failure to appear on date or hearing**.-

If on the date fixed for hearing, the appellant does not appear, he Appellate Officer may dismiss the appeal for default of appearance of the appellant.   
    

**36\. Restoration or appeals**.-

(i) Where an appeal has been dismissed under Rule 35 the appellant may apply to the Appellate Officer for the re-admission of the appeal and where it is proved that he was prevented by any sufficient cause from appearing when the appeal was called on for hearing the Appellate Officer shall restore the appeal on its original number.   
(ii) Such an application shall, unless the Appellate Officer extends the time for sufficient reason, be made within 30 days of the date of dismissal.   
    

**37\. Hearing or appeal**.-

(1) If the appellant is present when the appeal is called on for the hearing, the Appellate Officer shall proceed to hear the appellant or his authorised agent and any other person summoned by him for this purpose, and pronounce judgment on the appeal either confirming, reversing or varying the order appealed from.   
(2) The judgment of the Appellate Officer shall state the points for determination, the decisions thereon and reasons for the decisions.   
(3) The order shall be communicated to the appellant and copy thereof shall be sent to the Registering Officer or the Licensing Officer from whose order the appeal has been preferred.  
     

**38\. Payment or fees**.-

Unless otherwise provided in these rules all fees to be paid under these rules shall be paid in the local treasury under the head of account "XXXII-Miscellaneous-Social and Developmental Organisations-Labour and Employment-Fees under Contract Labour (Regulation and Abolition) Central Rules, 1971 (Central)", and  receipt obtained which shall be submitted with the application or the memorandum of appeal as the case may be.   
    

**39\. Copies**.-

Copy of the order of the Registering Officer, Licensing Officer or the Appellate Officer may be obtained on payment of fees of rupees two for each order on application specifying the date and other particulars of the order, made to the officer concerned.      
[Top](#Section)  
 

**CHAPTER V**  
**WELFARE AND HEALTH OF CONTRACT LABOUR**   
     

**40.**

(1) The facilities required to be provided under Sections 18 and 19 of the Act, namely, sufficient supply of wholesome drinking water, a sufficient number of latrines and urinals, washing facilities and first-aid facilities, shall be provided by the contractor in the case of the existing establishments within seven days or the commencement of these rules and in the case of new establishments within seven days of the commencement of the employment of contract labour therein.   
(2) If any of the facilities mentioned in sub-rule (1) is not provided by the contractor within the period prescribed the same shall be provided by the principal employer within seven days of the expiry of the period laid down in the said sub-rule.   
   

**41\. Rest-rooms**.-

(1) In every place wherein contract labour is required to halt at night in connection with the working of the establishment to which the Act applies and in which employment of contract labour is likely to continue for three months or more the contractor shall provide and maintain rest-rooms or other suitable alternative accommodation within fifteen days of the coming into force of the rules in the case of existing establishments, and within fifteen days of the commencement of the employment of contract labour in new establishments.   
(2) If the amenity referred to in sub-rule (1) is not provided by the contractor within the period prescribed, the principal employer shall provide the same within a period of fifteen days of the expiry of the period laid down in the said sub-rule.   
(3) Separate rooms shall be provided for women employees.   
(4) Effective and suitable provision shall be made in every room for securing and maintaining adequate ventilation by the circulation of fresh air and there shall also be provided and maintained sufficient and suitable natural or artificial lighting.   
(5) The rest-room or rooms or other suitable alternative accommodation shall be of such dimensions so as to provide at least a floor area or 1.1 sq. meter for each person making use of the rest-room.   
(6) The rest-room or rooms or other suitable alternative accommodation shall be so constructed as to afford adequate protection against heat, wind, rain and shall have smooth, hard and impervious floor surface.   
(7) The rest-room or other suitable alternative accommodation shall be at a convenient distance from the establishment and shall have adequate supply of wholesome drinking water.   
**NOTES**  
      Provisions of R. 41, held, are not unreasonable**.** _Gammon India Ltd. v. Union of India_, (1974)1 SCC 596.   
    

**42\. Canteens**.-

(1) In every establishment to which the Act applies and wherein work regarding the employment of contract labour is likely to continue for six months and wherein contract labour numbering one hundred or more are ordinarily employed an adequate canteen shall be provided by the contractor for the use of such contract labour within sixty days of the date of coming into force of the rules in the case of the existing establishments and within 60 days of the commencement of the employment of contract labour in the case of new establishments.   
(2) If the contractor fails to provide the canteen within the time laid down the same shall be provided by the principal employer within sixty days of the expiry of the time allowed to the contractor.   
(3) The canteen shall be maintained by the contractor or principal employer, as the case may be, in an efficient manner.   
[Top](#Section)  
**NOTES**  
     Provisions of R. 42, held, are not unreasonable_. Gammon India Ltd. v. Union of India_, (1974) 1 sac 596.   
     

**43**.

(1) The canteen shall consist of at least a dining hall, kitchen, store-room, pantry and washing places separately for workers and for utensils.   
(2) (i) The canteen shall be sufficiently lighted at all times when any person has access to it.   
     (ii) The floor shall be made of smooth and impervious material and inside walls shall be lime-washed or colour washed at least once in each year:  
      Provided that the inside walls of the kitchen shall be lime-washed every four months.   
(3) (i) The precincts of the canteen shall be maintained in a clean and sanitary condition.   
     (ii) Waste water shall be carried away in suitable covered drains and shall not be allowed to accumulate so as to cause a nuisance.   
     (iii) Suitable arrangements shall be made for the collection and disposal of garbage.   
    

**44\. Dining-hall**.-

(1) The dining-hall shall accommodate at a time at least 30 per cent of the contract labour working at a time.   
(2) The floor area of the dining-hall, excluding the area occupied by the service counter and any furniture except tables and chairs shall be not less than one square metre per diner to be accommodated as prescribed in sub- rule (1).   
(3) (i) A portion of the dining-hall and service counter shall be partitioned off and reserved for women workers, in proportion to their number .   
     (ii) Washing places for women shall be separate and screened to secure privacy.   
(4) Sufficient tables, stools, chairs or benches shall be available for the number of diners to be accommodated as prescribed in sub-rule (1).   
[Top](#Section)  
    

**45\. Furniture and utensils**.-

(1) (i) There shall be provided and maintained sufficient utensils, crockery, cutlery, furniture and any other equipment necessary for the efficient running of the canteen.   
     (ii) The furniture, utensils and other equipment shall be maintained a clean and hygienic condition.   
(2) (i) Suitable clean clothes for the employees serving in the canteen shall also be provided and maintained.    
     (ii) A service counter, if provided, shall have top of smooth and impervious material.   
     (iii) Suitable facilities including an adequate supply of hot water shall be provided for the cleaning of utensils and equipment.   
    

**46**.

The foodstuff, and other items to be served in the canteen shall be in conformity with the normal habits of the contract labour.  
[Top](#Section)  
   

**47\. Charges of Foodstuff**.-

The charges for foodstuffs, beverages and any other items served in the canteen shall be based on 'no profit, no loss' and shall be conspicuously displayed in the canteen.   
   

**48**.

In arriving at the prices of foodstuffs and other articles served in the canteen the following items shall not be taken into consideration as expenditure, namely-  
(a)     the rent for the land and building ;  
(b)     the depreciation and maintenance charges for the building and equipment provided for in the canteen ;  
(c)     the cost of purchase, repairs and replacement of equipments including furniture, crockery, cutlery and utensils ;  
(d)     the water charges and other charges incurred for lighting and  ventilation ;  
(e)     the interest on the amounts spent on the provision and maintenance of furniture and equipment provided for in the canteen.   
   

**49\. Books of Account**.-

The books of accounts and registers and other documents used in connection with the running of the canteen shall be produced on demand to an Inspector.   
   

**50\. Audit**.-

The accounts pertaining to the canteen shall be audited once every 12 months by registered accountants and auditors:   
     Provided that the Chief Labour Commissioner (Central) may approve of any other person to audit the accounts, if he is satisfied that it is not feasible to appoint a registered accountant and auditor in view of the site or the location of the canteen.    
   

**51\. Latrines and urinals**.-

Latrines shall be provided in every establishment coming within the scope of the Act on the following scale, namely-  
(a)     where females are employed, there shall be at least one latrine for every 25 females ;   
(b)     where males are employed, there shall be at least one latrine for every 25 males :   
    Provided that where the number of males or females exceeds 100, it shall be sufficient if there is one latrine for every 25 males or females, as the case may be, up to the first 100, and one for every 50 thereafter.   
**NOTES**  
     Provisions of R. 51, held, reasonable. _Gammon India Ltd. v. Union of India_, (1974) 1 SCC 596.    
    

**52**. Every latrine shall be under cover and so partitioned off as to secure privacy, and shall have a proper door and fastenings.   
    

**53**. (1) Where workers of both sexes are employed there shall be displayed outside each block of latrine and urinal a notice in the language understood by the majority of the workers "For Men Only" or "For Women only, as the case may be;   
      (2) The notice shall also bear the figure of a man or of a woman, as the case may be.   
   

**54**. There shall be at least one urinal for male workers up to 50 and one for female workers up to 50 employed at a time :   
    Provided that where the number of male or female workmen, as the case may be, exceeds 500 it shall be sufficient if there is one urinal for every 50 males or females up to the first 500 and one for every 100 or part thereof thereafter.    
    

**55**. (1) The latrines and urinals shall be conveniently situated and accessible to workers at all times at the establishment.   
      (2)(i) The latrines and urinals shall be adequately lighted and shall be maintained in a clean and sanitary condition at all times.   
          (ii) Latrines and urinals other than those connected with a flush sewage system shall comply with the requirement of the public health authorities.   
   

**56.** Water shall be provided by the means of tap or otherwise so as to be conveniently accessible in or near the latrine and urinals.   
   

**57**. **Washing facilities**. -

(1) In every establishment coming within the scope of the Act adequate and suitable facilities for washing shall be provided and maintained for the use of contract labour employed therein.   
(2) Separate and adequate screening facilities shall be provided for the use of male and female workers.   
(3) Such facilities shall be conveniently accessible and shall be kept it clean and hygienic condition.   
    

**58\. First-aid facilities**.-

(1) In every establishment coming within the scope of the Act there shall be provided and maintained so as to be readily accessible during all working hours First-Aid Boxes at the rate of not less than one box for 150 contract labour or part thereof ordinarily employed.   
    

**59.** (1) The First-Aid Box shall be distinctively marked with a red cross on a white ground and shall contain the following equipment, namely:   
(a)     For establishments in which the number of contract labour employed does not exceed fifty-   
Each First-Aid Box shall contain the following equipment-   
(i)6 small sterilized dressings  
(ii)3 medium size sterilized dressings  
(iii)3 large size sterilized dressings  
(iv)3 large sterilized burn dressings  
(v)1 (30 ml.) bottle containing a two per cent alcoholic solution of iodine  
(vi)1 (30 ml.) bottle containing salvolatile having the dose and mode of administration indicated on the label  
(vii)1 snake-bite lancet   
(viii)1 (30 gms.) bottle of potassium permanganate crystals  
(ix) 1 pair scissors  
(x)  1 copy of the First-Aid leaflet issued by the Director-General, Factory Advice Service and Labour Institutes, Government of India  
(xi) A bottle containing 100 tablets (each of 5 grains) of aspirin  
(xii) Ointment for burns  
(xiii)A bottle of suitable surgical antiseptic solution.  
 [Top](#Section)  
(b) For establishments in which the number of contract labour exceeds fifty-  
         Each First-Aid Box shall contain the following equipment.  
(i) 12 small sterilized dressings  
(ii) 6 medium size sterilized dressings  
(iii)6 large size sterilized dressings  
(iv)6 large size sterilized burn dressings  
(v)6 (15 gms.) packets sterilized cotton wool  
(vi)1 (60 ml.) bottle containing a two per cent alcoholic solution of iodine  
(vii)1 (60 ml.) bottle containing salvolatile having the dose and mode of administration indicated on the label  
(viii)1 roll of adhesive plaster  
(ix) A snake-bite lancet   
(x)1 (30 gms.) bottle of potassium permanganate crystals  
(xi)1 pair scissors   
(xii)1 copy of the First Aid leaflet issued by the Director-General, Factory Advice Service and Labour Institutes, Government of India  
(xiii) A bottle containing 100 tablets (each of 5 grains) of aspirin  
(xiv)Ointment for burns  
(xv)A bottle of a suitable surgical antiseptic solution.  
    (2) Adequate arrangement shall be made for immediate recoupment of the equipment when necessary.   
    **60**. Nothing except the prescribed contents shall be kept in the First-Aid Box.   
    **61**. The First-Aid Box shall be kept in charge of a responsible person who shall always be readily available during the working hours of the establishment.   
    **62**. A person incharge of the First-Aid Box shall be a person trained in First-Aid treatment, in establishments where the number of contract labour employed is 150 or more.  
 [Top](#Section)  
**CHAPTER VI**  
**WAGES**  
    **63**. The contractor shall fix wage periods in respect of which wages shall be payable.   
    **64.** No wage period shall exceed one month.   
    **65**. The wages of every person employed as contract labour in an establishment or by a contractor where less than one thousand such persons are employed shall be paid before the expiry of the seventh day and in other cases before the expiry of tenth day after the last day of the wage period in respect of which the wages are payable.   
    **66.** Where the employment of any worker is terminated by or on behalf of the contractor the wages earned by him shall be paid before the expiry of the second working day from the day on which his employment is terminated.    
    **67**. All payments of wages-shall be made on a working day of the work premises and during the working time and on a date notified in advance and in case the work is completed before the expiry of the wage period, final payment shall be made within 48 hours of the last working day.   
    **68**. Wages due to every worker shall be paid to him direct or to other person authorised by him in this behalf.   
    **69.** All wages shall be paid in current coin or currency or in both.   
   **70**. Wages shall be paid without any deductions or any kind except those specified by the Central Government by general or special order in this behalf or permissible under the Payment of Wages Act, 1936 (4 of 1936).   
     **71**. A notice showing the wage-period and the place and/time of disbursement of wages shall be displayed at the place of work and a copy   
     **72**. The principal employer shall ensure the presence of his authorized representative at the place and time of disbursement of wages by the contractor to workmen and it shall be the duty of the contractor to ensure the disbursement of wages in the presence of such authorised representative.   
    **73.** The authorised representative of the principal employer shall record under his signature a certificate at the end of the entries in the Register of Wages or the **13**\[Register of Wage-_cum_\-Muster Roll\], as the case may be, in the following form :  
 Certified that the amount shown in column No has been paid to the workman concerned in my presence on ... at  
  [Top](#Section)  
**CHAPTER VII**  
**REGISTERS AND RECORDS AND COLLECTION OF STATISTICS.**   
      **74.** **Registers of contractors**.- Every principal employer shall maintain in respect of each registered establishment a register of contractors in Form XII.   
     **75. Register of persons employed**.- Every contractor shall maintain in respect of each registered establishment where he employs contract labour a register in Form XIII.   
     **76. Employment card**.- (i) Every contractor shall issue an employment card in Form XIV to each worker within three days of the employment of the worker.   
    (ii) The card shall be maintained up-to-date and any change in the particulars shall be entered therein.   
    **77.** **Service certificate**.- On termination of employment for any reason whatsoever the contractor shall issue to the workman whose services have been terminated a Service Certificate in Form XV.   
    **78. Muster Roll, Wages Register, Deduction Register and Overtime** **Register**.- 14(\[(1) (a) Every contractor shall in respect of each work on which he engages contract labour,-   
    (i) maintain a Muster Roll and a Register of Wages in Form XVI and Form XVII respectively;   
     Provided that a combined Register of Wage-cum-Muster Roll in Form XVIII shall be maintained by the contractor where the wage period is a fortnight or less;  
 [Top](#Section)

13.

Subs. by GSR 948, dated 12-7-1978 (w.e.f. 22-7-1978).

14.

Subs. by GSR 948, dated 12-7-1978 (w. e. f. 22-7-1978) for sub-rules (1) and (2).

     (ii) maintain a Register of Deductions for damage or loss, Register of Fines and Register of Advances in Form XX, Form XXI and Form XXII respectively;   
    (iii) maintain a Register of Overtime in Form XXIII recording therein the number of hours of, and wages paid for, overtime work, if any.   
    (b) Every contractor shall, where the wage period is one week or more, issue wage slips in Form XIX, to the workmen at least a day prior to the disbursement of wages;   
    (c) Every contractor shall obtain the signature or thumb-impression of the worker concerned against the entries relating to him on the Register of Wages or Muster Roll-cum-Wages Register, as the case may be, and the entries shall be authenticated by the initials of the contractor or his authorised representatives and shall also be duly certified by the authorised representative of the principal employer in the manner provided in Rule 73.   
     (d) In respect of establishments which are governed by the Payment of Wages Act, 1936 (4 of 1936) and the rules made thereunder, or Minimum Wages Act, 1948 (11 of 1948) or the rules made thereunder, the following registers and records required to be maintained by a contractor as employer under those Acts and the rules made thereunder shall be deemed to be registers and records to be maintained by the contractor under these rules, namely:-    
(a)     Muster Roll;  
(b)      Register of Wages ;  
(c)      Register of Deductions ;  
(d)      Register of Overtime;  
(e)      Register of Fines ;  
(f)        Register of Advances ;  
(g)      Wage slip.\]   
      (3) Notwithstanding anything contained in these rules, where a combined or alternative form is sought to be used by the contractor to avoid duplication of work for compliance with the provisions of any other Act or the rules framed thereunder or any other laws or regulations or in cases where mechanised pay rolls are introduced for better administration, alternative suitable form or forms in lieu of any of the forms prescribed under these rules, may be used with the previous approval of the Chief Labour Commissioner (Central).    
     **79**. Every contractor shall display an abstract of the Act and rules in English and Hindi and in the' language spoken by the majority of workers in such form as may be approved by the Chief Labour Commissioner (Central).   
    **80**. (1) All registers and other records required to be maintained under the Act and rules, shall be maintained complete and up-to-date, and, unless otherwise provided for, shall be kept at an office or the nearest convenient building with, the precincts of the workplace or at a place within a radius of three kilometers.    
      (2) Such registers shall be maintained legibly in English or Hindi.   
       (3) All the registers and other records shall be preserved in original for period of three calendar years from the date of last entry therein.   
       (4) All the registers, records and notices maintained under the Act or rules shall be produced on demand before the Inspector or any other authority under the Act or any person authorised in that behalf by the Central Government.   
       (5) Where no deduction or fine has been imposed or no overtime has been worked during any wage period, a 'nil' entry shall be made across the body of the register at the end of the wage period indicating also in precise terms the wage period to which the 'nil' entry relates, in the respective registers maintained in Forms XX, XXI and XXII respectively.   
       **81.** (I) (i) Notices showing the rates of wages, hours of work, wage periods, dates of payment of wages, names and addresses of the Inspectors having jurisdiction, and date of payment of unpaid wages. shall be displayed in English and in Hindi and in the local language understood by the majority of the workers in conspicuous places at the establishment and the work-site by the principal employer or the contractor, as the case may be.   
     (ii) The notices shall be correctly maintained in clean and legible condition.   
    (2) A copy of the notice shall be sent to the Inspector and whenever any changes occur the same shall be communicated to him forthwith.   
      15\[(3) Every principal employer shall, within fifteen days of the commencement or completion of each contract work under each contractor, submit a return to the Inspector, appointed under Section 28 of the Act, intimating the actual dates of the commencement or, as the case may be, completion of such contract work, in Form VI-B.\]   
     **82. Returns**.- (1) Every contractor shall send half yearly return in Form XXIV (in duplicate) so as to reach the Licensing Officer concerned not later than 30 days from the close of the half year.   
       **Not**e.-Half year for the purpose of this rule means "a period of 6 months commencing from 1st January and 1st July of every year".   
      (2) Every principal employer of a registered establishment shall send annually a return in Form XXV (in duplicate) so as to reach the Registering Officer concerned not later than the 15th February following the end of the year to which it relates.   
     **83.** (1) The Board, Committee, Chief Labour Commissioner (Central) or the Inspector or any other authority under the Act shall have the powers to call for any information or statistics in relation to contract labour from any contractor or principal employer at any time by an order in writing.   
    (2) Any person called upon to furnish the information under sub-rule (1) shall be legally bound to do so.  
 

15.

Ins. by GSR 199, dated 25-1-1977 (w.e.f. 12-2-1977).

 [Top](#Section) 

**FORM I**  
 (See Rule 17(1)\]   
_Application for Registration of Establishments Employing Contract Labour_   
1.       Name and location of the Establishment.  
2.       Postal address of the Establishment.  
3.       Full name and address of the principal Employer (furnish father's name in the case of individuals).  
4.       Full name and address of the Manager or person responsible for the supervision and control of the Establishment.  
5.       Nature of work carried on in the Establishment.  
6.       Particulars of Contractors and Contract Labour :  
(a)     Names and Addresses of Contractors.  
(b)      Nature of work in which contract labour is employed or is to be employed.  
(c)     Maximum number of contract labour to be employed on any day through each contractor.  
16(cc) Estimated date of commencement of each contract work under each contractor.\]  
(d)     Estimated date of termination of employment of contract labour under each contractor.  
   
7.       Particulars of Treasury Receipt enclosed (Name of the Treasury, Amount and Date).  
   
I hereby declare that the particulars given above are true to the best of my knowledge and belief.  
Principal Employer.  
Seal and Stamp  
                                                                                                                                                             
   
Date of receipt of                                                                              _Office of the Registering_  
  application                                                                                                                _Officer._  
 [Top](#Section)

* * *

**FORM II**  
\[See Rule 18(1)\]  
_Certificate of Registration_  
No.                                                                                                                   Date  
Government of India  
Office of the Registering Officer  
    A Certificate of Registration containing the following particulars is hereby granted under sub section (2) of Section 7 of the Contract Labour (Regulation and Abolition) Act, 1970, and the rules made thereunder, to.....  
            1. Nature of work carried on in the Establishment.  
            2. Names and addresses of Contractors.  
            3. Nature of work in which Contract Labour is employed or is to be employed.  
            4. Maximum number of Contract Labour to be employed on any day through each Contractor.  
            5. Other particulars relevant to the employment of Contract Labour.  
                                                                                                                        _Signature of Registering_  
                                                                                                                               _Officer with Seal_.

16.

Ins. By GSR 199, dated 25-1-1977 (w.e.f. 12-2-1977).

 [Top](#Section) 

* * *

   
**FORM III**  
\[See Rule 189(3)\]  
_Register of Establishments_

Sl No.

Registration No, and date

Name and address of the establishment  
registered

Name of the Principal Employer and his address

Type of business, trade, industry, manufacture or occupation, which is carried on in the establishment

Total No. workmen directly employed

1

2

3

4

5

6

 

 

 

   
 

 

 

   
  

_Particular of Contractor and Contract Labour_

Name and address of contractor

Nature of work in which Contractor Labour is employed

Maximum No. of Contractor Labour to be Employed on any day

Probable duration of employment of Contract Labour

Remarks

7

8

9

10

11

 

 

   
 

 

 

 [Top](#Section)

* * *

    
**FORM IV**  
\[See Rule 21 (1)\]  
_Application for Licence_  
 1.       Name and address of the contractor (including his father's name in case of individuals).   
2.       Date of birth and age (in case of individuals).   
3.       Particulars of Establishment where Contract Labour is to be employed-   
(a)     Name and address of the Establishment ;  
(b)     Type of business, trade, industry, manufacture or occupation, which is carried on in the Establishment ;  
(c)     Number and date of Certificate of Registration of the Establishment under the Act ;  
(d)      Name and address of the Principal Employer.   
4.       Particulars of Contract Labour-   
(a)     Nature of work in which Contract Labour is employed or is to be employed in the Establishment ;  
(b)     Duration of the proposed contract work (give particulars of proposed date of commencing and ending) ;  
(c)     Name and address of the Agent or Manager of Contractor at the work-site ;  
(d)      Maximum number of Contract Labour proposed to be employed  on the Establishment on any date;   
5.       Whether the contractor was convicted of any offence within the preceding five years. If so, give details.  
   
6.       Whether there was any order against the contractor revoking or suspending licence or forfeiting security deposits in respect of an earlier contract. If so, the date of such order.  
 7.        Whether the contractor has worked in any other establishment within the past five years. If so, give details of the Principal Employer, Establishment, and nature of work.   
8.       Whether a certificate by the Principal Employer, in Form V is enclosed.   
9.        Amount of licence fee paid-under of Treasury Challan and date.   
17\[10. Particulars of security deposit, if any, requested to be adjusted, including treasury number and date.   
The amount of security or balance, if any, after adjustment of amount to be refunded under Rule 31, if any, deposited with Treasury Receipt number and date.\]   
_Declaration_.- I hereby declare that the details given above are correct to the best of my knowledge and belief.  
_Signature of the Applicant_  
_(Contractor)_  
Place .  
Date  
   
**Note.-**The application should be accompanied by a Treasury Receipt for the appropriate amount and a certificate in Form V from the Principal Employer.  
                                                                                                                                                             
(To be filled in the office of the Licensing Officer)  
Date of receipt of the application with challan for fees/Security Deposit  
_Signature of the Licensing Officer._  
[Top](#Section)

* * *

**FORM V**  
\[See Rule 21(2)\]  
_Form of Certificate by Principal employer_  
             
            Certified that I have engaged the applicant (name of the contractor) as a contractor in my establishment. I Undertake to be bound by all the provisions of the Contract Labour (Regulation and Abolition) Act, 1970 and the Contract Labour (Regulation and Abolition Central Rules, 1971, insofar as the provisions are applicable to me in respect of the employment of Contract Labour by the applicant in my establishment.  
   
Place                                                                       _Signature of Principal Employer_  
Date.                                                           Name and address of Establishment  
   
 [Top](#Section)

* * *

   
**18\[FORM VA**  
\[See Rule 21(2)\]  
_Application for Adjustment of Security Deposit_  
                                                                                                                                   

Name and address of the Contractor

No. and date of licence

Date of expiry of previous licence

Whether the licence of the contractor was suspended or revoked

(1)

(2)

(3)

(4)

 

 

 

 

   
 

No. and date of the treasury receipt of the security deposit in respect of the previous licence

Amount of previous security deposit

No. and date of treasury receipt of the balance security deposit, if any, required on the fresh contract

(5)

(6)

(7)

 

 

   
 

No. and date of certificate of registration of the establishment in related to which the new licence is applied for

Name and address of the principal employer

Particulars of fresh contract

Remarks

(8)

(9)

(10)

(11)

 

 

 

   
 

   
Place:  
Date:                                                                                                    _Signature of the Applicant._\]  
 [Top](#Section)

* * *

   
**FORM VI**  
\[See Rule 25(1)\]  
Government of India  
_Office of Licensing Officer_

Licence No.                              

Dated

            Fee paid Rs ..

   
   
**LICENCE**  
      1. Licence is hereby granted to under Section 12(1) of the Contract Labour (Regulation and Abolition) Act, 1970, subject to the conditions specified in Annexure).  
   
       19\[2. This licence is for doing the work of (nature of work to be indicated) in the establishment of- (name of principal employer to be indicated) at- (place of work to be indicated).  
   
     3. The licence shall remain in force till- (date to be indicated.\]  
                                     
Date                                                            _Signature and Seal of Licensing Officer_  
   
RENEWAL  
\[See Rule 29\]

Date of renewal

Fee paid for renewal

Date of expiry

1.

 

 

2.

 

 

3.

 

 

   
 

 

 

   
Date                                                                _Signature and Seal of the Licensing Officer_  
  

19.

Ins. by GSR 948, dated 12-7-1978 (w.e.f. 22-7-1978).

   
**ANNEXURE**  
The licence is subject to the following condition  
(1)     The licence shall be non-transferable.   
(2)     The number of workmen employed as Contract Labour in the establishment shall not, on any day, exceed .   
(3)     Except as provided in the rules the fees paid for the grant, or as the case may be, for renewal of the licence shall be non-refundable.   
(4)     The rates of wages payable to the workmen by the contractor shall not be less than the rates prescribed for the Schedule of employment under the Minimum Wages Act, 1948, where applicable, and where the rates have been fixed by agreement, settlement or award, not less than the rates fixed.   
(5)      In case where the workmen employed by the contractor by the contractor perform the same or similar kind of work as the workmen directly employed by the principal employer of the establishment, the workmen of the contractor shall be the same and applicable to the workmen directly employed by the principal employer of the establishment on the same similar kind of work provided that in the case of any disagreement with regard to the type of work the same shall be decided by the Chief Labour Commissioner (Central), whose decision shall be final.     
(6)     In other cases the wage rates, holidays, hours of work and conditions of service of the workmen of the contractor shall be such as may be specified in this behalf by the Chief Labour Commissioner (Central).   
(7)     In every establishment where 20 or more women are ordinarily employed as contract labour there shall be provided two rooms of reasonable dimensions for the use of their children under the age of six years. One of such rooms would be used as a play-room for the children and the other as bed-room for the children. For this purpose the contractor shall supply adequate number of toys and games in the play-room and sufficient number of cots and beddings in the sleeping-room. The standard of construction and maintenance of the creches may be such as may be specified in this behalf by the Chief Labour Commissioner (Central).  
   
(8)     The licensee shall notify any change in the number of workmen or the conditions of work to the Licensing Officer.  
   
20\[(9) A copy of the licence shall be displayed prominently at the premises where the contract work is being carried on.\]  
 [Top](#Section)

* * *

**21\[FORM VI-A\]**  
\[See Rule 25(2)(viii)\]  
_Notice of commencement/completion of contract work_  
   
            I/We Shri/M/s.  (Name and address of the contractor) hereby intimate that the contract work (Name of work) in establishment of (Name and address of principal employer) for which licence No , dated .. has been issued to me/us by the licencing officer (name of the headquarter), has been commenced/completed with effect from (date)/on (date).  
   
                                                                                                                        _Signature of Contractor_  
To  
            The Inspector,  
            ..  
            ..  
   
 

20.

Ins. by GSR 948, dated 12-7-1978 (w.e.f. 22-7-1978).

21.

Ins. by GSR 199, dated 25-1-1977 (w.e.f. 12-2-1977)

[Top](#Section)

* * *

   
**FORM VI-B**  
\[See Rule 81(3)\]  
_Notice of commencement/completion of contract work_  
(1)     Name of the principal employer and address ..  
   
(2)     No. and date of Certificate of registration  
   
(3)     I/We hereby intimate that the contract work (Name of work) given to (name and address of the contractor) having licence No dated . has been commenced completed with effect from (date)/on (date).  
    
                                                                                                _Signature of the Principal Employer_  
   
To  
            The Inspector,  
             
             
[Top](#Section)

* * *

**FORM VII**  
\[See Rule 29(2)\]  
_Application for Renewal of Licence_  
   
1.       Name and address of the contractor.  
 2.       Number and date of the licence.  
 3.       Date of expiry of the previous licence.  
 4.       Whether the licence of the contractor was suspended or revoked.  
 5. Number and date of the treasury receipt enclosed.  
   
Place...  
                                                                                                             
_Signature of the Applicant_  
Date  
   
                                                                                                                                                             
(To be filled in the Office of the Licensing Officer)  
Date of receipt of the application with  
Treasury Receipt No. and date.                                                   _Signature of the Licensing Officer_  
   
 [Top](#Section)

* * *

**FORM VIII**  
\[See Rule 32(2)\]  
_Application for Temporary Registration of Establishments Employing Contract Labour_  
1.       Name and location of the establishment.  
 2.       Postal address of the establishment.   
3.       Full name and address of the Principal employer (furnish fathers name in the case of individuals).  
4.       Full name and address of the Manager or person responsible for the supervision and control of the establishment.  
 5.       Nature of work carried on in the establishment,  
 6.       Particulars of contract labour:  
A.      Nature of work in which contract labour is to be employed and reasons for urgency.  
B.      Maximum number of contract labour to be employed on any day.  
C.      Estimated date of termination of employment of contract labour.  
7.       Particulars of treasury receipt 22\[\* \* \*\] enclosed  
  

22**.**

_Omitted_ by Noti. No. GSR 870, dated 26-7-1974 (w.e.f. 10-8-1974).

             
I hereby declare that the particulars given above are true to the best of my knowledge and belief.  
                                                                                                                        _Principal Employer_  
                                                                                                                                    Seal and Stamp  
                                                                                                                                                             
Time and date of receipt of application with  
Treasury Receipt 23\[\* \* \*\]                                                             _Office of the Registering Officer_  
   
 [Top](#Section)

* * *

**FORM IX**  
\[See Rule 32(3)\]  
_Temporary Certificate of Registration_  
                                                                                                            _Date of Expiry_  
No.                                                                                                                        Date  
Government of India  
Office of the \[Registering Officer\]24  
                    A Temporary Certificate of Registration containing the following particulars is hereby granted under sub-section (2) of Section 7 of the Contract Labour (Regulation and Abolition) Act, 1970, and the rules made thereunder, to.. Valid from to  
            1. Nature of work carried on in the establishment.  
             2. Nature of work in which Contract Labour is to be employed.  
             3. Maximum number of Contract Labour to be employed on any day.  
             4. Other particulars relevant to the employment of Contract Labour.  
                                                                                       _Signature of Registering officer with Seal_  
 [Top](#Section)

* * *

**FORM X**  
\[See Rule 32(2)\]  
_**Application for Temporary Licence**_  
   
1.       Name and address of the contractor  (including his fathers name in case of individuals).  
 2.       Date of birth and age (in case of individuals).  
 3.       Particulars of  Establishment where Contract Labour is to be employed-  
(a)     Name and address of the Establishment                     
(b)     Type of business, trade, industries manufacture or occupation, which is carried on in the establishment ;  
(c)     Name and address of the Principal Employer.  
 4.       Particulars of Contract Labour-  
(a)     Nature of work in which Contract Labour is to be employed in the establishment;  
(b)     Duration of the proposed contract work (give particulars of proposed date of commencing and ending);  
(c)     Name and address of the agent or Manager of Contractor at the work-site;  
(d)     Maximum number of Contract Labour proposed to be employed in the establishment on any day.

23.

Omitted by Noti. No. GSR 870, dated 26-7-1974 (w.e.f. 10-8-1974).

24.

Subs. by GSR 1649, dated 16-12-1972 (w.e.f. 30-12-1972).

5.       Whether the contractor was convicted of any offence within the preceding five years. If so, give details.  
 6.       Whether there was any order against the contractor revoking or suspending licence or forfeiting security deposits in respect of an earlier contract. If so, the date of such order.  
 7.        Whether the contractor has worked in any other establishment within the past five years. If so, give details of the Principal Employer, Establishments and nature of work.  
 8.        Amount of licence fee paid-Number of Treasury Challan 25\[\* \* \*\] and date.  
 9.       Amount of Security deposit-Treasury Receipt 25\[\* \* \*\] number and date.  
       I hereby declare that the particulars given above are true to the best of my knowledge and belief.  
   
Place                                                                                       _Signature of the Applicant_  
Date.                                                                                                      (Contractor)  
                                                                                                                                                             
(To be filled in the Office of the Licensing Officer)  
Date of receipt of the application with challan fee for Security Deposit.  
                                                                                                             
                                                                                                _Signature of the Licensing Officer_  
 [Top](#Section)

* * *

**FORM XI**  
\[See Rule 32(3)\]  
Government of India  
_Office of the Licensing Officer_  
Licence No.                                                       Dated                            Fee paid Rs  
                                                                                                _Signature of the Licensing Officer_  
            Temporary Licence                                                         Expires on  
   
            Licence is hereby granted to .. under  Section 12(2) of the Contract Labour (Regulation and Abolition) Act, 1970, subject to the conditions specified in Annexure.  
   
The Licence shall remain in force till  
   
Date                                                       _Signature and Seal of the Licensing Officer_  
 [Top](#Section)  
**ANNEXURE**  
             
The license is subject to the following conditions :-  
(1)     The licence shall be non-transferable.  
(2)     The number of workmen employed as Contract labour in the establishment shall not, on any day, exceed .  
(3)     Except as provided in the rules the fees paid for the grant of the licence shall be non-refundable.  
(4)     The rates of wages payable to the workmen by the contractor shall not be less than the rates prescribed for the Schedule of employment under the Minimum Wages Act, 1948, where applicable, and where the rates have been fixed by agreement, settlement or award, not less than the rates fixed.  
(5)      In cases where the workmen employed by the contractor perform the same or similar kind of work as the workmen directly employed by the principal employer of the establishment, the wage rates, holidays, hours of work and other conditions of service of the workmen of the contractor shall be the same as applicable to the workmen directly employed by the principal employer of the establishment on the same or similar kind of work: Provided that in the case of any disagreement with regard to the type of work the same shall be decided by the Chief Labour Commissioner (Central), whose decision shall be final.  
 [Top](#Section)

25.

Omitted by Noti. No. GSR 870, dated 26-7-1974 (w.e.f. 10-8-1974).

  (6)     In other cases the wage rates, holidays, hours of work and conditions of service of the workmen of the contractor shall be such as may be specified in this behalf by the Chief labour Commissioner (Central).  
 26\[(7) A copy of the licence shall be displayed prominently at the premises where the contract work is being carried on.\]  
 

* * *

**FORM XII**  
\[See Rule 74\]  
_Register of Contractors_  
1.       Name and address of the Principal Employer ...  
   
2.       Name and address of the establishment

 

 

 

 

Period of contract

 

Sl. No.

Name and address of contractor

Nature of work on contract

Location of contract work

From

To

Maximum No. of workmen employed by contractor

 

 

 

 

 

   
 

 

            [Top](#Section)

* * *

**FORM XIII**  
\[See Rule 75\]  
_Register of Workmen Employed by Contractor_

Name and address of Contractor

Name and address of establishment in/ under which

...

contract is carried on ..

 

 

 

 

Nature and location of work .

Name and address of Principal Employer ..

...

 

Sl. No.

Name and surname of workmen

Age and Sex

Father's/  
Husband's name

Nature of Employment/ Designation

Permanent Home Address of workmen (Village and Tahsil/ Taluk and District)

1

2

3

4

5

6

 

 

 

 

 

   
 

   
   
   
 

Local Address

Date of Commencement of employment

Signature or thumb-impression of workmen

Date of termination of employment

Reasons for termination

Remarks

7

8

9

10

11

12

 

 

 

 

 

   
 

   
 

26.

 Ins by GSR 948, dated 12-7-1978 (22-7-1978).

   
 [Top](#Section)

* * *

**FORM XIV**  
\[See Rule 76\]  
_Employment Card_

Name and address of Contractor

Name and address of Establishment in under which

...

contract is carried on

 

..

Nature of work and location of work

Name and address of Principal Employer .

...

..

1.

Name of workmen

..

 

..

..

2.

Sl. No. in the register of workmen

 

 

employed            ..

..

 

..

..

3.

Nature of employment/Designation

..

..

4.

Wages rate (with particular of unit, in case

 

 

of piece-work)

..

 

..

..

5.

Wage period       ..

..

 

..

..

6.

Tenure of employment

..

 

..

..

7.

Remarks             ..

..

 

..

..

_Signature of Contractor_

   
 [Top](#Section)

* * *

**FORM XV**  
\[See Rule 77\]  
_Service Certificate_

Name and address of contractor

..

Name and address of establishment in/

 

 

under which contract is carried on

 

 

 

 

 

 

 

 

Nature and location of work

..

..

 

Name and address of the wor-

 

Name and address of Principal Employer

man

..

 

..

 

..

..

........

 

 

........

Age or Date of Birth

..

..

 

Identification Marks

 

..

 

Father's / Husband's name

..

..

 

 

Total period which employed

 

 

 

Sl. No.

From

To

Nature of work done

Rate of wages (with particulars of unit in case of piece work)

Remarks

1

2

3

4

5

6

   
 

 

 

 

 

 

                                                                                                            _Signature...._  
   
   
 [Top](#Section)

* * *

**FORM XVI**  
 \[See Rule 789(1) (a)(i)\]  
 Muster Roll

Name and address of contractor

..

Name and address of establishment in/

 

 

under which contract is carried on

 

 

 

Nature and location of work

..

..

Name and address of Principal Employer

 

 

 

 

..

For the month of .....

 

 

 

 

 

 

 

 

 

 

Dates

 

Sl. No.

Name of workman

Father's / Husband's name

Sex

1 2 3 4 5

Remarks

 

 

 

 

   
 

 

   
 [Top](#Section)  
 

* * *

**FORM XVII**  
\[See Rule 78(1) (a)(i)\]  
_Register of Wages_

Name and Address of Contractor

Name and address of Establishment in / under

...

which contract is carried on .

Nature and location of works

...

 

 

...

Name and address of Principal

 

Employer .

 

Wage period: Monthly ...

   
 

Sl. No.

Name of workman

Serial No. in the register of workman

Designation / nature of work done

No. of days worked

Units of works done

1

2

3

4

5

6

   
 

 

 

 

 

 

   
 

 

Amount of wages earned

Daily-rate of wages/piece rate

Basic wages

Dearness Allowances

Overtime

Other cash payments (Nature of payment to be indicated)

Total

7

8

9

10

11

12

   
 

 

 

 

 

 

Deductions, if any, (indicate nature)

Net amount paid

Signature / Thumb impression of workman

Initial of contractor or his representative

13

14

15

16

 

 

 

   
 

 [Top](#Section)

* * *

**FORM XVIII**  
\[See Rule 78(1)(a)(i)\]  
_Form of Register of Wages-cum-Muster Roll_

Name and address of Contractor

Name and address of establishment in under which

...

contract is carried on .

 

Name and address of Principal Employer .

 

...

 

Wage period: Weekly/Fortnightly

Nature and location of work

From to ...

...

 

 

 

 

 

Daily attendance units worked

 

Sl. No.

Sl. No. in Register of workmen

Name of employee

Designation/ nature of work

1  2 **. .** 15

Total attendance / units of work done

1

2

3

4

5

6

 

 

 

 

 

   
 

   
 

 

Amount of wages earned

Daily-rate of wages/piece-rate

Basic Wages

Dearness allowance

Overtime

Other cash payment (nature of payments to be indicated)

Total

7

8

9

10

11

12

 

 

 

 

 

   
 

   
 

Deduction, if any, (indicate nature)

Net amount paid

Signature / Thumb impression of workman

Initials of contractor or his representative

13

14

15

16

   
 

 

 

 

   
 [Top](#Section)  
   
 

* * *

    
**FORM XIX**  
\[See Rule 78(1)(b)\]  
_Wage Slip_

Name and address of Contractor

Name and Father's / Husband's name of the

...

workman .

 

..

Nature of work and location of work

For the Week/Fortnight/Month ending ...

...

..

...

..

1.

No. of days worked

..

 

..

 

2.

No. of units worked in case of piece

..

 

rate workers        ..

..

 

..

..

3.

Rate of daily wages/piece-rate

..

..

4.

Amount of overtime wages

 

..

..

5.

Gross wages payable

..

 

..

..

6.

Deductions, if any

..

 

..

..

7.

Net amount of wages paid

..

 

..

..

 

 

 

 

 

 

_Initials of the Contractor or his_  
Representative

* * *

**FORM XX**  
\[See Rule 78(1) (a)(ii)\]  
_Register of Deductions for Damage or Loss_

Name and address of Contractor

Name and Address of Establishment in / under

..

which contract is carried on .

 

...

Name and Location of work .

Name and Address of Principal Employer

...

...

Sl. No.

Name of work

Father's / Husband's name

Designation/ Nature of Employment

Particulars of damage or loss

Date of Damage or loss

Whether workman showed cause against deduction

1

2

3

4

5

6

7

   
 

 

 

 

 

 

 

 

 

 

Date of recovery

 

Name of person in whose presence employee's was heard

Amount of deduction imposed

No. of instalments

First instalments

Last instalments

Remarks

8

9

10

11

12

13

   
 

 

 

 

 

 

 [Top](#Section)

* * *

   
**FORM XXI**  
\[See Rule 78(1)(a)(ii)\]  
_Register of Fines_  
 

Name and address of Contractor

Name and Address of Establishment in / under

..

which contract is carried on .

 

...

Name and Location of work .

Name and Address of Principal Employer

...

...

Sl.

Name workman

Father's/ Husband name

Designation / nature for fine imposed

Act/ Omission for which imposed

Date of offence

1

2

3

4

5

6

   
 

 

 

 

 

 

   
 

Whether workman showed cause against fine

Name of person in whose presence employee's explanation was heard

Wage periods and wages payable

Amount of fine imposed

Date on which fine realised

Remarks

7

8

9

10

11

12

   
 

 

 

 

 

 

   
 [Top](#Section)  
 

* * *

**FORM XXII**  
   
\[See Rule 78(1)(a)(ii)\]  
   
_Register of Advances_

Name and address of Contractor

Name and Address of Establishment in / under

..

which contract is carried on .

 

...

Name and Location of work .

Name and Address of Principal Employer

...

...

Sl. No.

Name

Father's / Husband's name

Nature of employment/ Designation

Wage period and wages payable

Date and amount of advance given

1

2

3

4

5

6

 

 

 

 

 

   
 

Purpose (s) for which advance made

No. of instalments by which advance to be repaid

Date and amount of each instalments repaid

Date on which last instalments was repaid

Remarks

7

8

9

10

11

 

 

 

   
 

 

 [Top](#Section)

* * *

   
**FORM XXIII**  
   
\[See Rule 78(1)(a) (iii)\]  
   
_Register of Overtime_  
 

Name and address of Contractor

Name and Address of Establishment in / under

..

which contract is carried on .

 

...

Name and Location of work .

Name and Address of Principal Employer

...

...

Sl. No.

Name of workman

Father's / Husband's name

Sex

Designation/ nature of employment

Date on which overtime worked

1

2

3

4

5

6

 

 

 

 

 

   
 

Total overtime worked or production in case of piece-rated

Normal rates of wages

Overtime rate of wages

Overtimes earnings

Date on which overtime wages paid

Remarks

7

8

9

10

11

12

   
 

 

 

 

 

 

 [Top](#Section)

* * *

**FORM XXIV**  
\[See Rule 82(1)\]  
_Return to be sent by the Contractor to the Licensing Officer_  
 

1.

Name and address of the Contractor

..

Half-Year-Ending ..

2.

Name and address of the establish-

 

 

 

ment

..

 

..

 

..

 

3.

Name and address of the Principal

 

 

 

employer

..

 

..

 

..

 

4.

Duration of Contract: From ..

 

 

. to

 

5.

No. of days during half year on which-

 

(a)

the establishment of the Principal

 

employer had worked

(b)

the contractor's establishment had

 

worked ..                ..                      ..

6.

Maximum number of contract labour emp-

 

 

loyed on any day during the half year:

 

 

_Men_

_Women_

 

_Children_

 

_Total_

 

7.

(i) Daily hours of work and spread over-

 

 

(ii)

_(a)_

whether weekly holiday

 

 

 

 

observed and on what day-

 

 

 

_(b)_

If so, whether it was paid for-

 

 

(iii)

No. of man-hours of overtime work-

 

 

 

ed-

 

8.

Number of man-days worked by-

 

 

_Men_

_Women_

 

_Children_

 

_Total_

 

9.

Amount of wages paid-

 

 

_Men_

_Women_

 

_Children_

 

_Total_

 

10.

Amount of deduction from wages, if any-

 

 

_Men_

_Women_

 

_Children_

 

_Total_

 

11.

Whether the following have been  provided-

 

 

(i)

Canteen

..

 

..

..

 

 

(ii)

Rest-Room

 

 

..

..

 

 

(iii)

Drinking water

 

 

..

..

 

 

(iv)

Crches

..

 

..

..

 

 

(v)

First-Aid

..

 

..

..

 

 

 

(If the answer is 'yes' state briefly standard provided)

 

Place

 

_Signature of Contractor_

 

Date ..

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   
 [Top](#Section)  
 

* * *

   
**FORM XXV**  
\[See Rule 82(2)\]  
_Annual Return of Principal Employer to be sent to the Registering Officer_  
                                     
Year ending 31st December          
1.       Full name and address of the Principal Employer.  
2.       Name of Establishment:  
_(a)_     District  
_(b)_    Postal Address  
_(c)_     Nature of operation/industry/work carried on.  
3.       Full name of the Manager or person responsible for supervision and control of the establishment.  
4.       Number of Contractors who worked in the establishment during the year (Give details in Annexure).  
5.       Nature of work/operations on which contract labour was employed.  
6.       Total number of days during the year on which contract labour was employed.  
7.       Total number of mandays worked by contract labour during the year.  
8.       Maximum number of workmen employed directly on any day during the year.  
9.       Total number of days during the year on which direct labour was employed.  
10.   Total number of mandays worked by directly employed workmen.  
11.   Change, if any, in the management of the establishment, its location, or any other particulars furnished to the Registering Officer in the application for Registration indicating also the dates.  
   
                                                             
                                                                                                                        _Principal Employer_  
Place .  
   
Date ...  
   
**ANNEXURE TO FORM**  
 

Name and Address of the Contractor

Period of contract  
   
From-To

Nature of work

Maximum number of workers employed by each contractor

No. of days worked

No. of mandays worked

1

2

3

4

5

6

   
 

 

 

 

 

 

   
 [Top](#Section)

* * *

**Contract Labour (Regulation and Abolition) Central Rules, 1971-Construction and**  
**Maintenance of Creches**  
   
_Notification No. S.O. 143, dated the 8th September, 1972\*_  
 [Top](#Section)  
         In exercise of the powers conferred by clause (vi) (d) of sub-rule (2) of Rule 25 of the Contract Labour (Regulation and Abolition) Central Rules, 1971, the Chief Labour Commissioner (Central), New Delhi hereby specifies the standard of construction and maintenance of the Crches as under:

 

 

**1\. Locations**.- A Creche should be located within 50 metres of every establishment where 20 or more women are ordinarily employed as contract labour. While the Crche should be conveniently accessible to the mothers of the children accommodated therein, it should not be situated in close proximity to establishment where obnoxious fumes, dust or odours are given off or in which excessively noisy processes are carried on.   
**2\. Building**.- (i) The Creche building should be constructed of heat resistant materials and should be rain-proof.   
(ii) While in towns it may be built of brick walls with cement or lime plaster, in rural areas it may be built of mud walls with mud plaster. In either case, the flooring and the walls up to a height of 3 ft. should have cement surface.   
     (iii) The height of the rooms should be not less than 10 ft. from the floor to the lowest part of the roof.   
     (iv) The rooms should be provided with necessary doors and windows for securing and maintaining adequate light and ventilation by free flow of air.   
     (v) The building should be periodically inspected in order to see that it is safe and is being maintained under sanitary conditions.   
     (vi) The Crche will be kept open at all times both by day and night, when women employees are working.   
     **3. Accommodation**.- (i) Accommodation in the Crche should be on a scale of at least 20 sq. ft. of floor area per child.   
     (ii) There should be a shady open air play-ground suitably fenced for older children.   
       **4. Amenities.-** (i) Cool and wholesome drinking water should be available for the children and the staff of the Crche. Children below 2 years of age should be fed with at least 1/2 pint of pure milk per child per day. Children above 2 years of age should be given wholesome refreshments.   
     (ii) Convenient and suitable arrangements should be made for the working mothers to feed their children below 2 years of age during the intervals.   
     (iii) There shall be a kitchen attached to the. Crche with utensils and other facilities for boiling milk and preparing refreshments, etc.   
     (iv) The children as well as the staff of the Crche should be provided with suitable uniforms for wear at the Crche.  
     (v) There should be a suitable bathroom adjoining the Crche for the washing, of the children and for changing their clothes. Wash basins or similar vessels should also be provided it the rate of one for every four children. There should be arrangements for supply of water at the rate of 5 gallons per child per day. Adequate supply of clean towels and soap should be available at the Crche:  
   (vi) Adjoining the bathroom there shall be a latrine for the exclusive use of the children in the Creche. The number of seats in the latrine shall be at the rate of one for every 15 children. Separate latrines should be maintained for the use of mothers and Creche staff at a distance of not less than 50 ft. from the Creche.  
 [Top](#Section)  
      **5. Equipment**.- The Creche should have the following equipment at the rate of one for each child-   
(i)                   Cradles or Cots.  
(ii)                 Beds or mattresses.  
(iii)                Cotton sheets.  
(iv)                Rubber sheets (for children below 3 years).  
(v)                  Blankets.  
(vi)                Pillow with covers.  
   
      **6. Staff**.- Every Creche should be in the charge of a woman with mid-wifery qualification or training as Creche attendant. Where the number of children exceeds ten, the Creche attendant should be assisted by female ayahs at the rate of one ayah-  
(a)     for every 5 children up to one year ;  
(b)     for every 10 children up to three years; and  
(c)     for every 15 children of over 3 years of age.  
 [Top](#Section)  
The ayahs should not be less than 30 years of age and should have knowledge and training in the handling of children.   
        **7. Working hours**.- The working hours of the Creche should correspond to the working hours of the mothers. It may have to work in two shifts if the women are employed in two or more shifts, spread over a periods exceeding 8 hours a day.  Where the Creche works in shifts, different staff should be employed to work in the two shifts.   
         **8. Medical attention**.- (i) The Creche should have first-aid equipment kept in proper condition.   
       (ii) Every child should be medically examined before admission. There should be medical check-up of the children once a month and their weight recorded once a month.    
        (iii) A record of the periodical medical check-up and weighment should be entered in the record of medical examination of each child kept at the Creche.   
       **9. Maintenance or records**.- The Creche should maintain the following records up-to-date-  
(i)                   Records of Medical Examination of children, in Form "A".   
(ii)                 Attendance Register of children, in Form "B".   
       **10. Inspection or Creche**.- A Creche may be inspected at any time by an Inspector under the Act or any other officer authorised by the Central Government for the purpose. 

\*

Published in Gazette of India, dated 13-1-73, Pt. 11, S. 3(ii). p. 158.

   
   
 

* * *

**Form A**  
_Form for Recording the Result of Medical Examination of Children Attending Creches_  
_Date, Month and year of Examination .._

Sl. No.

Name of child

Age (date of birth, if available)

Mother's name and occupation

Weight of child on the date of last examination

Weight on the date of examination

Disease or abnormality found, if any

Treatment suggested, if any

Remarks

1

2

3

4

5

6

7

8

9

 

 

 

 

 

   
 

 

 

 

                         
                                                                        _(Signature of the qualified medical practitioner)_  
   
   
 [Top](#Section)

* * *

   
**Form B**  
   
_Form for Recording Particulars of Children Attending the Creches_

Name of Establishment .

Month and Year

Sl. No.

Date of admission

Name of child with mother's full name and occupation

Sex

Age

Date of the month (attendance marked each day)

Remarks

1

2

3

4

5

6

7
