Maharashtra Contract Labour (Regulation and Abolition) Rules,
1971.

Maharashtra Contract Labour (Regulation and
Abolition) Rules , Maharashtra Contract Labour Rules ,
1971
Section 1: Short title
These rules may be called the Maharashtra Contract Labour (Regulation and Abolition) Rules,
1971.
Section 2: Definitions
In these rules, unless the subject or context otherwise requires-
(a) "Act" means the Contract Labour ( Regulation and Abolition) Act, 1970,
(b) "Appellate Officer" means the Officer appointed as such by Government under sub-section
(1) of section 15,
(c) "Board " means the state Advisory Contract Labour Board, constituted under section 4,
(d) "Chairman" means the Chairman of the Board;
(e) "Committee" means a Committee constituted under sub- section (1) of section 5;
(f) " Form" means a form appended to these rules;
(g) "Government " means the Government of Maharashtra;
(h) "Licensing Officer" means an Officer notified as such under section 11;
(i) "Registering Officer" means an Officer notified a such under section 6;
(j) "Section" means a section of the Act.
Chapter: State Advisory Board
Section 3: Constitution of Board
The Board shall consist of the following members:-
(a) a Chairman to be appointed by Government;
(b) the Commissioner of Labour, ex officio or in his absence any other Officer nominated by
Government in this behalf;
(c) one person representing the State Government to be appointed by Government;
(d) five persons, three representing the principal employers and two representing
contractors to whom the Act applies, to be appointed by the Government;
(e) five persons representing workmen to whom the Act applies to be appointed by Government.
Chapter: State Advisory Board
Section 4: Term of Office
(1) The Chairman of the Board shall hold office for a period of three years from the date on
which his appointment is first notified in the Official Gazette.
(2) The member of the Board , referred to in clause (c) of rule 3 , shall hold office during
the pleasure of the Government.
(3) Each of the members referred to in clauses ( d) and (e) of rule 3 shall hold office for
a period of three years commencing from the date on which his appointment is first notified
in the Official Gazette:
Provided that, Where the successor of any such member has not been notified in the Official
Gazette on or before the expiry of the said period of three years, such member shall,
notwithstanding the expiry of the period of his office, continue to hold such office until
the appointment of his successor has been notified in the Official Gazette.
Chapter: State Advisory Board
Section 5: Resignation
(1) A Member of the Board , not being an ex-officio member may resign his office by a letter
in writing addressed to the Government.
(2)The office of a member of the Board, shall fall vacant from the date on which his
resignation is accepted by the Government or on the expiry of thirty days from the date of
receipt of intimation of resignation whichever is earlier].
Chapter: State Advisory Board
Section 6: Cessation of membership
If any member of the Board , not being an ex-officio member , fails to attend three
consecutive meetings of the Board , without obtaining the leave of the Chairman for such
absence, he shall cease to be a member of the Board:
Provided that Government may , if it is satisfied that such member was prevented by
sufficient cause from attending three consecuting meetings of the Board , direct that such
cessation shall not take place and on such direction being made, such member shall continue
to be a member of the Board.
Chapter: State Advisory Board
Section 7: Disqualification of membership
(1) A person shall be disqualified for being appointed and for being a member of the Board
(i) if he is of unsound mind and stands so declared by a competent court ; or
(ii) if he is an undischarged insolvent;
or
(iii) if he has been or is convicted of an offence which in the opinion of the Government
involves moral turpitude.
(2) If a question arises as to whether or not a disqualification has been incurred under
sub-rule (1) the Government shall decide the same
Chapter: State Advisory Board
Section 8: Removal from membership
Government may remove any member of the Board from office , if in its opinion such a member
has ceased to represent the interest which he purports to represent on the Board:
Provided that no such member shall be removed unless a reasonable opportunity is given to
him of making any representation against the proposed action.
Chapter: State Advisory Board
Section 9: Vacancy
(1) When a vacancy occurs or is likely to occur in the membership of the Board , the
Chairman shall submit a report to the Government and on receipt of such report , the
Government shall take steps to fill the Vacancy.
(2) If any vacancy occurs in the membership of the Board by reason of death of, or
resignation by a member , the vacancy caused thereby shall be filled by the Government by
making an appointment from amongst the category of persons to which the deceased or as the
case may be the resigned member belonged and the person so appointed shall hold office for
the remainder of the term of office of the member in whose place he is appointed.
Chapter: State Advisory Board
Section 10: Staff
(1) (I) Government may appoint one of its officials as Secretary of the Board and appoint
such other staff as it may think necessary to enable the Board to carry out its functions.
(ii) The salaries and allowances payable to the staff and the other conditions of service of
such staff be such a may be decided by the Government.
(2) The Secretary,
(i) shall assist the Chairman in convening meetings of the Board;
(ii) may attend the meeting , but shall not be entitled to vote at such meetings.
(iii) Shall keep a record of the minutes of such meetings; and
(iv) Shall take necessary measures to carry out the decisions taken at the meetings of the
Board.
Chapter: State Advisory Board
Section 11: Allowances of members
(1) The travelling allowance of an official member shall be governed by the rules applicable
to him for journey performed by him on official duties and shall be paid by the authority
paying his salary.
(2) The non-official members of the Board shall be eligible to draw travelling allowance and
daily allowance for any journey performed by them in connection with work of the Board in
accordance with scale I specified n rule I (1) (b) of appendix XLIIA-A, Section I, to the
Bombay Civil Services Rules, 1959. The non - official members, who are ordinarily residents
of the place at which the meeting of the Board is held, shall be eligible to draw conveyance
charges not exceeding rupees five only per sitting for every day of the meeting of the
Board, which they attend. The secretary for the Boards shall be the Controlling Authority in
respect of travelling daily and conveyance allowance bills of the non - official members.
Chapter: State Advisory Board
Section 12: Disposal of business
Every question which the Board is required to take into consideration shall be considered at
a meeting , or if the Chairman so directs, by sending the necessary papers to every member
for opinion, and the question shall be disposed of in accordance with the decision of the
majority:
Provided that in the case of equality of votes, the Chairman shall have a second or casting
vote.
Chapter: State Advisory Board
Section 13: Meeting
(1) The Board shall meet at such places and times as may be specified by the Chairman.
(2) The Chairman shall preside over every meeting of the Board at which he is present and in
his absence a member elected by those present at the meeting shall preside over such
meeting.
Chapter: State Advisory Board
Section 14: Notice of meeting and list of business
Ordinary seven days notice of a proposed meeting shall be given to the members.
Provided that , when an emergent meeting is called by the Chairman, it shall not be
necessary to give more than three days notice.
(2) No business which is not on the agenda of a meeting shall be considered at that meeting
without the permission of the Chairman.
Chapter: State Advisory Board
Section 15: Quorum
No Business shall be transacted at any meeting unless the Chairman and at least one third
member other than Chairman are Present;
Provided that , if at any meeting less than such number if members are present the Chairman
may adjourn the meeting to another date informing members present and giving notice to the
other members that he proposes to dispose of the business at the adjourned meeting whether
there is quorum or not , and it shall thereupon be lawful for him to dispose of the business
at the adjournment meeting irrespective of the number of members attending.
Chapter: State Advisory Board
Section 16: Committee of the Board
(1) The Board may, while constituting the committees under section 5 .nominate one of its
members to be the Chairman if the Committee.
[(2) The Committee shall meet at such times and place as the Chairman of the said Committee
may decide].
Chapter: State Advisory Board
Section 16A: Notice of meeting
A notice of the date, time and place of the meeting. Along with the agenda for the meeting,
shall be sent to each member by registered post at least fifteen days before the date fixed
for such meeting:
Provided that in the case of an emergent meeting, such notice and the agenda for the meeting
shall be sent at least seven days before the date fixed for such meeting.]
Chapter: State Advisory Board
Section 16B: Presiding over meeting of Committees
The Chairman of the Committee shall preside at every meeting of the Committee at which he is
present and in his absence, the members present shall elect one of their member, to preside
at that meeting.
Chapter: State Advisory Board
Section 16C: Quorum
(1) The quorum to constitute a meeting of a Committee shall be as near as may be , one third
of the total number of members of the Committee.
(2) If at any time fixed for meeting of the Committee , or if at any time during any such
meeting , there is no quorum , the presiding authority shall either adjourn the meeting
until there is quorum or adjourn the meeting to some future day.
(3) When the meeting has been adjourned in pursuance of sub- rule (2) on two successive
dates fixed for the meeting of the Committee , it will be lawful to dispose of at the next
adjourned meeting the business to be transacted at the original meeting , irrespective the
number of members of the Committee present at such meeting.
Chapter: State Advisory Board
Section 16D: Voting in the Committee
(1) All questions at a meeting of the Committee shall be determined by a majority of the
members present and voting.
(2) In the case of an equality of votes of any matter , the presiding authority shall have a
second or casting vote.
(3) Voting shall be by show of hands.
Chapter: State Advisory Board
Section 16E: Proceeding of the meeting
(1) The proceedings of each meeting showing inter alia the names of the members present
thereat shall be forwarded to each member and to the Board as soon as after the meeting as
possible and in any case, not less than seven days the next meeting.
(2) The proceedings of each meeting shall be confirmed with such modification, if any , as
may be considered necessary at the next meeting.
(3) The Committee shall meet at such times and places as the Chairman of the said Committee
may decide and the Committee shall observe such rules of procedure in regard to the
transaction of business at its meeting as it may decided upon.
(4) The provisions of rule 11 shall apply to the members of a Committee for attending the
meeting of the Committee as they apply to the members of the Board.
Chapter: Registration and licensing
Section 17: Manner of making application for registration of establishments
(1) The application referred to in sub-section (1) of Section 7 shall be made in triplicate
in Form No 1 to the registering officer of the area in which the establishment sought to be
registered is located.
(2) Every such application shall be accompanied by a treasury receipt showing payment of the
fees for the registration of the establishment according to the provisions of Rule 26; and
shall be either personally delivered to the registering officer or sent to him by registered
post.
(3) On receipt of the application referred to in sub-rule (1), the registering officer shall
record thereon the date of receipt by him of the application; and shall acknowledge the
receipt thereof.
Chapter: Registration and licensing
Section 18: Gran of certificate of registration
(1) The certificate of registration granted under sub-section (2) of Section 7 shall be in
From No.II
(2) The registering officer shall maintain a register in From No. III showing the
particulars of establishment in relation to which certificates of registration have been
issued by him.
(3) If, in relation to an establishment, there is any change in the particulars specified in
the certificate of registration, the principal employer of the establishment shall intimate
to the registering officer, within fifteen days from the date when such change takes place,
the particulars of, and the reasons for such change takes place, the particulars of, and the
reasons for such change.
Chapter: Registration and licensing
Section 19: Circumstances in which application for registration may be rejected
(1) If any application for registration is not complete in all respects, registering officer
shall require the principal employer to amend the application so as o make it complete in
all respects.
(2) If the principal employer, on being required by the registering officer to amend his
application for registration, omits or fails to do so, within fifteen days from the date of
receipt of an intimation from the registering officer, the registering officer shall reject
the application for registration.
Chapter: Registration and licensing
Section 20: Amendment of certificate of registration
(1) Where, on receipt of the intimation under sub-rule (3) of Rule 18, the registering
officer is satisfied that an amount higher than the amount which has been paid by the
principal employer as fees for the registration of the establishment is payable, he shall
require such principal employer to deposit a sum which, together with the amount already
paid by such principal employer, would be equal to such higher amount of fees payable for
the registration of the establishment and to produce the treasury receipt showing such
deposit.
(2) Where, on receipt of the intimation referred to in sub-rule (3) Rule 18, the registering
officer is satisfied that there has occurred a change in the particulars of the
establishment, as entered in the register in Form No. III he shall amend the said register
and record therein and in the certificate of registration in Form II the change which has
occurred:
Provided that no such amendment shall affect anything done or any action taken or any right,
obligation or liability acquired or incurred before such amendment:
Provided further that the registering officer shall bot carry out any amendment in the
register in Form III unless the appropriate fees have been deposited by the principal
employer.
Chapter: Registration and licensing
Section 21: Application for a licence
(1) Every application by a contractor for the grant of a licence shall be made in triplicate
in Form No. IV, to the licensing officer of the area in which the establishment, in relation
to which he is the contractor, is located.
(2) Every application for the grant of a licence shall be accompanied by a certificate from
the principal employer in Form No. V to the effect that the applicant has been employed or
is proposed to be employed by him as contractor in relation to his establishment and that he
undertakes to be bound by all the provisions of the Act and rules made thereunder in respect
of employment of contract labour by the applicant.
(3) Every such application shall be either personally delivered to the licensing officer or
sent to him by registered post.
(4) On receipt of the application the Licensing Officer shall record thereon the date of
receipt of the application and acknowledge the receipt thereof.
(5) Every application shall be accompanied by a treasury receipt showing-
(i) the deposit of the security at the rates specified in Rule 24; and
(ii) the payment of the fees at the rates specified in Rule 26.
Chapter: Registration and licensing
Section 22: Matters to be taken into account in granting or refusing a licence
In granting or refusing to grant a licence, the licensing officer shall take the following
matters into account, namely:-
(a) whether the applicant-
(i) is a minor; or
(ii) is of unsound mind and stands so declared by a competent court; or
(iii) is an undischarged insolvent; or
(iv) has been convicted (at any time during a during a period of five years immediately
preceding the date of application) of an offence which, in the opinion of the Government,
involves moral turpitude;
(b) whether there is an order of Government or an award or settlement for the abolition of
contract labour in the establishment in relation to which the applicant is a contractor;
(c) whether any order has been made in respect of the applicant under sub-section (1) of
Section 14, and if so, whether a period of three years has elapsed from the date of that
order;
(d) whether the fees for the application have been deposited at the rates specified in Rule
26; and
(e) whether security has been deposited by the applicant at the rates specified in Rule 24.
Chapter: Registration and licensing
Section 23: Grant of licence
(1) On receipt of the application, and as soon as possible thereafter, the licensing officer
shall make such enquiry as he considers necessary to satisfy himself about the eligibility
of the applicant for licence.
(2) Where the licensing officer is of opinion that the licence should not be granted, he
shall, after affording reasonable opportunity to the applicant to be heard, and after
recording his reasons make an order rejecting the application and the order refusing the
licence shall be communicated to the applicant.
Chapter: Registration and licensing
Section 24: Security
Before a licence is issued, an amount calculated at the rate of Rs.20 for each workman to be
employed as contract labour, in respect of which the application for licence has been made,
shall be deposited as security by the contractor for due performance of the conditions of
the licence and compliance with the provisions of the Act or the rules made thereunder:
[Provided that, where the contractor is a co-operative society, the amount to be deposited
as security shall be at the rate of Rs.10 for each workman to be employed as contract
labour.]
Chapter: Registration and licensing
Section 25: Form and terms and condition of licence
(1) Every licence granted under Rule 23 or renewed under Rule29 shall be in No. VI.
(2) Every such licence shall be subject to the following conditions, namely:-
(i) the licence shall be non-transferable;
(ii) the number of workmen employed as contract labour in the establishment shall not, no
any day, exceed the maximum number specified in the licence;
(iii) save as provided in these rules, the fees paid for the grant ,or as the case may be ,
for renewal of the licence shall be non-refundable;
(iv) (a) The rate of wages payable to the workmen by a contractor shall not be less than the
minimum rates of wages fixed under the Minimum Wages Act, where that Act applies , where the
rates have been fixed by agreement, settlement or award shall not be less than the rates so
fixed , and where rates have been fixed under the Minimum Wages Act and also under any
agreement, settlement or award, the rates, shall not be less than the higher of the two
rates
(b) where , the workmen employed by the contractor perform the same kind of work as the
workmen as a class of workmen directly employed by the principal employer, the rates of
wages payable to the workmen by the contractor shall be the rates payable to the workmen
directly employed by the principal employer doing the same kind of work.
(c) in any other case , the rates of wages shall be such as may be specified in this behalf
by the Commissioner of Labour
(v) (a)the hours of work and other conditions of service of the workmen of the contractor
shall be in accordance with the provisions of the Minimum Wages Act , where that Act
applies, where any agreement, settlement or award is in force in accordance with the
provisions of the said agreement ,settlement or award; and where in any employments the
Minimum Wages Act applies and there is also in force any agreement ,settlement or award, the
conditions of service shall be governed by the provisions which are more beneficial to the
workmen;
(b) in other cases where the workmen employed by a contractor perform the same kind of work
as the workmen directly employed by the principal employer of an establishment, the hours of
work and other conditions of service of the workmen of the contractor shall be the same as
applicable to the workmen directly employed by the principal employer of the establishment;
(c) in cases not falling under sub-clause (a) or (b) the hours of work and other conditions
of service of the workmen of the contractor shall be such as may be specified by the
Commissioner of Labour.
Explanation.- while determining the wages , hours of work and other conditions of service
under sub-clause (c) of clause (iv) and sub-clause (c) of clause (v) the Commissioner of
Labour shall have due regard to the wages, hours of work and other conditions of service
obtaining in similar employment:
(vi) (a) in every establishment , where twenty or more women are ordinarily employed as
contract labour, there shall be provided and maintained by the contractor a room or rooms
for use of children under the age of six years as may be required by the Commissioner of
Labour and the standard of construction, scale of accommodation and the facilities shall be
such as may be specified by the Commissioner of Labour:
Provided that where the principal employer is required under the Factories Act and the Rules
thereunder to provide and maintain a creche (or other alternative arrangements for the use
of children of women employees directly employed by him, any arrangements made by the
contractor with the principal employer for the use of the creche for other alternative
arrangements in lieu of creche) by the children under age of six years of the female workmen
employed by the contractor shall be considered as due compliance of the provisions of this
clause:
Provided further that such arrangements according to the standard prescribed in the
Factories Act and the Rules framed thereunder;
(b) in other cases, there shall be provided and maintained a room or rooms for the use of
children under the age of six years, as may be specified by the Commissioner of Labour .
(vii) the licensee shall notify any change in the number of workmen or the conditions of
work to the licensing officer.
(viii) The licensee shall, within seven days of the commencement and completion of each
contract work give an intimation in Form VI-A to the Inspector appointed under Section 28,
intimating the actual date of Commencement or, as the case may be, of completion of each
contract work.
(ix) there shall not be any sexual harrasment to the women contract labour which includes
unwelcome sexual determined behaviour (whether directly or by implication) namely:-
(i) physical contact and advances; or
(ii) a demand or request for sexual favours; or
(iii) sexually coloured remarks; or
(iv) showing pornography; or
(v) any other unwelcome physical, verbal or non-verbal conduct of sexual nature
Chapter: Registration and licensing
Section 26: Fees
(1) The fees to be paid for the grant of a certificate of registration under section 7 shall
be as specified below, namely:-
If the number of workmen employed or proposed to be employed on contract on any day;
Rs.
(a) is 20 .. 200
(b) exceeds 20 but does not exceed 50 .. 500
(c) exceeds 50 but does not exceed 100 ..1000
(d) exceeds 100 but does not exceed 200 ..2000
(e) exceeds 200 but does not exceed 400 ..4000
(f) exceeds 400 ..5000
(2) The fees to be paid for the grant or renewal of a licence under section 12 shall be as
specified below:-
If the number of workmen employed or proposed to be employed by the contractor on any day-
Rs
(a) is 20 ..100
(b) exceeds 20 but does not exceed 50 ..250
(c) exceeds 50 but does not exceed 100 ..500
(d) exceeds 100 but does not exceed 200 ..1000
(e) exceeds 200 but does not exceed 400 ..2000
(f) exceeds 400 ..2500
Chapter: Registration and licensing
Section 27: Validity of licence
Every licence granted under Rule 23 or renewed under Rule 29 shall remain in force upto 31st
December of the year for which the licence is granted or renewed.
Chapter: Registration and licensing
Section 28: Amendment of licence
(1) A licence issued under Rule 23 or renewed under Rule 29 may, for good and sufficient
reasons, be amended by the licensing officer.
(2) The contractor who desires to have the licence amended shall submit to the licensing
officer an application stating the nature of the amendment and reason therefor.
(3) (I) If the licensing officer allows the application he shall require the applicant to
furnish a Treasury receipt for the amount, if any, by which the fees that would have been
payable if the licence had been originally issued in the amended from exceeds the fees
originally paid for the licence.
(ii) On the applicant furnishing the requisite Treasury receipt the licence shall be
according to the orders of the licensing officer.
(4) Where the applicant for amendment is refused, the licensing officer shall record his
reasons for such refusal and communicate the same to the applicant.
Chapter: Registration and licensing
Section 29: Renewal of licences
(1) Every contractor shall apply to the licensing officer for renewal of the licence in Form
No. VII, in triplicate not less than sixty days before the date on which the licence expires
, and if the application is so made , the licence shall be deemed to have been renewed until
such date when the renewal is granted or refused.
(2) The fees chargeable for renewal of the licence shall be the same for the grant thereof:
Provided that if the application for renewal is not received within the time specified in
sub - rule (2),a fee of 25 per cent in excess of the fee ordinarily payable for the licence
shall be payable for such renewal:
Provided further that in case where the licensing officer is satisfied that the delay in
submission of the application is due to unavoidable circumstances beyond the control of the
contractor , he may reduce of remit as he thinks fit the payment of such excess fee.
Chapter: Registration and licensing
Section 30: Issue of duplicate certificate of registration or licence
Where a certificate of registration or a licence granted or renewed under the preceding
rules has been lost, defaced or accidentally destroyed , a duplicate may be granted on
payment of a fee of rupees fifty.
Chapter: Registration and licensing
Section 31: Refund of security
(1)(i) On expiry of the period of licence the contractor may , if he does not intend to have
his licence renewed, make an application to the Licensing officer for the refund of the
amount deposited as security by him under rule 24.
(b) If the Licensing Officer is satisfied that there is no breach of the conditions of
licence or there is no order under section 14 for the forfeiture of the sum or any portion
thereof deposited as security, he shall direct the refund of the sum to the applicant
(2) If there is any order directing the forfeiture of any portion of the security deposit ,
the amount to be forfeited shall be deducted from the security deposit , and balance if any,
refunded to the applicant.
(3) Any application for refund shall , as far as possible ,be disposed of within 60 days of
the receipt of the application.
Chapter: Appeals and Procedure
Section 32: Appeals
(1) Every appeal under sub-section (1) of Section 15 shall be preferred in the form of a
memorandum signed by the appellant or his authorised agent and presented to the Appellate
Officer in person or sent to him by registered post.
The memorandum shall be accompanied by a certified copy of the order appealed from.
(2) The memorandum shall set forth concisely and under distinct head the grounds of appeal
to the order appealed from.
Chapter: Appeals and Procedure
Section 33: Rejection or returning of appeals
(1) Where the memorandum of appeal does not comply with the provisions of sub-rule (2) of
rule 32, it may be rejected or returned to the appellant for the purpose of being amended
within a time to be fixed by the Appellate Officer.
(2) Where the Appellate Officer rejects the memorandum under sub-rule (1) he shall record
the reason for such rejection, and shall communicate the order to the appellant.
(3) Where the memorandum of appeal is in order, the Appellate Officer shall admit the
appeal, endorse thereon the date presentation and shall register the appeal in a book to be
kept for the purpose called the Register Appeals.
(4) When the appeal has been admitted, the Appellate Officer shall send the notice of the
appeal to the Registering Officer or the Licensing Officer as the case may be from whose
order the appeal has been preferred and such officer shall send the record of the case to
the Appellate Officer.
On receipt of the record , the Appellate Officer shall send a notice to the Appellant to
appear before him on such date and at such time as may be specified in the notice for the
hearing of the appeal.
Chapter: Appeals and Procedure
Section 34: Rejection or returning of appeals
If on the date fixed for hearing , the appellant does not appear, the aqppellate Officer may
dismiss the appeal for default of appearance of the appellant.
Chapter: Appeals and Procedure
Section 35: Re -admissions of appeals
(1) Where an appeal has been dismissed under rule 34 the appellant may apply to the
Appellate Officer for the re - admission of the appeal, and where it is proved that he was
prevented by any sufficient cause from appearing when the appeal was called on for hearing
the appellate Officer shall restore the appeal on its original number.
(2) Such an application shall, unless the Appellate Officer extend the time for sufficient
reason, be made within 30 days of the date of dismissal.
Chapter: Appeals and Procedure
Section 36: Re - admissions of appeals
(1) If the appellant is present when the appeal is called on for hearing , the Appellate
Officer shall proceed to hear the appellant or his authorised agent and any other person
summoned by him for this purpose, and pronounce judgment on the appeal, either confirming,
reversing or varying the order appealed from.
(2) The judgment of the Appellate Officer shall state the point for determination, the
decisions thereon and the reasons for the decisions.
(3) The order shall be communicated to the appellant and copy thereof shall be sent to the
Registering Officer or the Licensing Officer from whose order the appeal has been preferred.
Chapter: Appeals and Procedure
Section 37: Payment of fees
All fees to be paid under these rules shall be paid in the local Treasury under the head of
account [Fees under the Contract and letter 087, Labour and Employment (f) fees under
Contract Labour ( Regulation and Abolition) Act. 1970 and a receipt obtained which shall be
submitted with the application or the memorandum of appeal as the case may.]
Chapter: Appeals and Procedure
Section 37A: Refund of fees
(1) Where an application for the grant of a certificate of registration or for the grant or
renewal of a licence or for the grant of a duplicate copy of a certificate of registration
or of a licence is rejected, or an application for amendment of a certificate of
registration or of a licence is not allowed the fees paid is respect thereof shall be
refunded to the applicant by the registering officer or, as the case may be, by the
licensing officer.
(2) Where any application as is referred to is sub-rule (1) is granted, any amount paid by
the applicant is excess of the prescribed fees shall be refunded:
Provided that, where any such excess amount of fees is paid at the time of renewal of a
licence, such amount may be adjusted against the fees for the next renewal of the licence.
Chapter: Appeals and Procedure
Section 38: Copies
Copies of the order of the Registering Officer, Licensing Officer or as the case may be the
Appellate Officer can be obtained on payment of rupees two for each order and on an
application specifying the date of the order, made to the officer concerned.
Chapter: Welfare and Health of Contract Labour
Section 39: Rest-rooms
(1) In every place wherein contract labour is required to halt at night in connection with
the working of a establishment to which the Act applies and in which employment of contract
labour is likely to continue for 3 months or more the contractor shall within sixty days of
the coming into force of the rules in the case of existing establishments, and within thirty
days of commencement of the employment of contract labour is new establishments provide and
maintain rest rooms or other suitable alternative accommodation as may be required by the
Commissioner of Labour and the scale of accommodation and facilities shall be such as may be
specified by the Commissioner of Labour:
Provided that where the principal employer of an establishment is required to provide rest
rooms for the workers directly employed by him in accordance with the provisions of the
Factories Act, any arrangements made by the Principal employer for the use of the rest rooms
by the workmen employed by the contractor shall be considered as due compliance of the
provisions of the clause:
Provided further that such arrangements are according to the standards prescribed in the
Factories Act and the Rules framed thereunder.
If the amenity referred to in sub-rule (1) is not provided by the contractor within the
period prescribed, the principal employer shall provide the same within period of thirty
days of the expiry of the period laid down in the said sub-rule.
Chapter: Welfare and Health of Contract Labour
Section 40: Canteens
(1) In every establishment to which the Act applies and wherein work regarding the
employment of contract labour is likely to continue for six months and wherein contract
labour numbering one hundred or more are ordinarily employed, the contractor shall within
sixty days of the date of coming into force of the rules in the case of the existing
establishments and within sixty days of the commencement of employment of contract labour in
the case of new establishments provide canteen or canteens as may be require by the
Commissioner of Labour and the construction, accommodation, furniture and foodstuffs and the
pricing thereof, shall be such as may be specified by the Commissioner of Labour.
Provided that where the employer of the establishment is required under the Factories Act to
provide for the workers directly employed by him canteen or canteens, any arrangements made
by the contractor with the principal employer so that the canteen shall also be available to
the workers employed by the contractor shall be considered as due compliance of the
provisions of this clause:
Provided further that such arrangements are according to the standards prescribed in the
Factories Act and the Rules framed thereunder.
If the contractor fails to provide the canteen within the time laid down the same shall be
provided by the principal employer within sixty days of the expiry of the time allowed to
the contractor.
The canteen shall be maintained by the contractor or principal employer, as the case may be,
in an efficient manner.
The books of accounts and registers and other documents used in connection with the running
of the canteen shall be produced on demand to an Inspector.
Chapter: Welfare and Health of Contract Labour
Section 41: Latrines and Urinals
(1) The contractor shall provide in every establishment coming within the scope of the Act
at least one latrine for every 20 persons up to the first 100, and one for every 30
thereafter.
Every latrine shall be under cover and so partitioned off as to secure privacy and shall
have a proper door and fastenings.
Where workers of both sexes are employed, there shall be displayed outside each block of
latrine and urinal a notice in the language understood by the majority of the workers "For
Men Only" or "For Women Only" as the case may be.
The notice shall also bear the figure of a man or of a woman, as the case may be.
There shall be at least one urinal for male workers upto fifty and one more for female
workers upto fifty employed at a time.
The latrines and urinals shall be conveniently situated and accessible to workers at all
time at the establishment.
(i) The latrines and urinals shall be adequately lighted and shall be maintained in a good
sanitary condition at all times.
(ii)Latrines and urinals other than those connected with a flush sewage system shall comply
with the requirements of the public health authorities.
Water shall be provided by the means of tap or otherwise in or near the latrines and urinals
so as to be conveniently accessible.
Nothing in sub-rule (1) to (7) shall apply where the principal employer of an establishment
as required under the Factories Act has provided latrines and urinals for the workers
directly employed by him and the contractor has arranged with the principal employer for use
of these latrines and urinals for workmen employed by the contractor.
If the contractor fails to provide the latrines and urinal or make arrangements as
prescribed in this rule within sixty days of the of coming into force of these rules in the
case of the existing establishments and within thirty days of the commencement of employment
of contract labour in the case of new establishments, the same shall be provided by the
principal employer within thirty days of the expiry of the period of sixty days given to by
the contractor.
Chapter: Welfare and Health of Contract Labour
Section 42: Washing facilities
(1) In every establishment coming within the scope of the Act the contractor shall provide
and maintain adequate and suitable facilities for washing for the use of contract labour
employed therein.
(2)Separate and adequate screening facilities shall be provided for the use of male and
female workers.
(3)Such facilities shall be conveniently accessible and shall be kept and clean and hygienic
condition.
(4)The washing facilities shall include the provisions of adequate number of buckets and
tumblers or mugs and water supply at the rate of 20 liters a day for each workmen employed.
Chapter: Welfare and Health of Contract Labour
Section 43: Other facilities
(1) The facilities required to be provided under sections 18 and 19 , namely sufficient
supply of whole-some drinking water, a sufficient number of latrines and urinals, washing
and first-aid facilities, shall be provided by the contractor in the case of the existing
establishment within sixty days of the commencement of these rules and in the case of new
establishments within thity days of the commencement of the employment of contract labour
therein.
If any of the facilities mentioned in sub-rule (1) is not provided by the contractor within
the prescribed period the same shall be provided by the principal employer within thiry days
of the expiry of that period.
Chapter: Welfare and Health of Contract Labour
Section 44: First-Aid boxes
(1) In every establishment coming within the scope of the Act the contractor shall provide
and maintain, so as to be readily accessible during all working hours, First- Aid boxes at
the rate of not less than one box for every 150 contract labour or part thereof ordinarily
employed.
The First-Aid box shall be marked distinctively with a red- cross on a white background and
shall contain the following equipment, namely:-
For establishments in which the number of contract labour employed does not exceed fifty-
Each First-Aid box shall contain the following equipments:
i. 6 small sterilized dressings.
ii. 3 medium size sterilized burn dressings.
iii. 3 large size sterilized burn dressings.
iv. 3 large sterilized burn dressings.
v. 1 (30 ml) bottle containing a tow percent alcoholic solution of iodine.
vi. 1 (30 ml) bottle containing salvolatile having the do and mode of administration
indicated on the label.
vii. 1 snake-bite lancet.
viii. 1 (30 gms) bottle of potassium permanganate crystals.
ix. 1 pair of scissors.
x. 1 copy of the first-aid leaflet issued by the director General, Factory Advise Sevide and
Labour Institute, Government of India.
xi. A bottle containing 100 tablets (each of 5 grams) of aspirin.
xii. Ointment for burns.
xiii. A bottle of suitable surgical antiseptic solution.
B. For establishments in which the number of contract labour exceed fifty-Each First-Aid box
shall contain the following equipments;-
i. 12 small sterilized dressings.
ii. 6 medium size sterlized dressings.
iii. 6 large size sterilized dressings.
iv. 6 large size sterilized burn dressings.
v. 6 (15gms) packets sterilized cotton wool.
vi. 1 (60 ml.) bottle containing a two percent alcoholic solution iodine.
vii. 1 ( 60 ml.) bottle containing salvolatile having the do and mode of administration
indicated on the label.
viii. 1 roll of adhesive plaster.
ix. A snake-bite lancet.
x. 1 (30 gms.) bottle of potassium permanganate crystals.
xi. 1 pair of scissors.
xii. 1 copy of first-aid leaflet issued by the Director General, Factory Advise Service and
Labour Institute, Government of India.
xiii. A bottle containing 100 tablets (each of 5 grams) of aspirin.
xiv. Ointment of burns.
xv. A bottle of suitable surgical antiseptic solution.
(3) Adequate arrangement shall be made of immediate recoupment of the equipment, when
necessary.
(4) Nothing except the prescribed contents shall be kept in the First-Aid Box.
(5) The First-Aid Box shall be kept in charge of a separate responsible person who shall
always be readily available during the working hours of the establishment.
(6) A person in charge of the First-aid Box shall be a person trained in First-Aid
treatment, in establishments where the number of contract labour employed is 15 or more
Chapter: Wages
Section 45: Wage period
The contractor shall fix wage periods in respect of which wages shall be payable, and no
wage period shall exceed one month.
Chapter: Wages
Section 46: Payment of Wages, how made
Wages of every worker shall be paid within three days from the end of the wage period in
case the wage period is one week or a fortnight and in all other cases before the expiry of
the tenth or the seventh day from the end of the wage period according to the number of
workers employed in such establishments does or does not exceed a thousand.
Chapter: Wages
Section 47: Payment of wages on termination
Where employment of any worker is terminated by or on behalf of the contractor the wages
earned by him shall be paid before the expiry of the day succeeding the one on which his
employment is terminated.
Chapter: Wages
Section 48: Payment to be made on working day
All payments of wages shall be made on a working day at the work site and during working
time and on a date notified in advance. In case the work is completed before the expiry of
the wage period, final payment shall be made within 48 hours of the last working day.
Chapter: Wages
Section 49:
Wages due to every worker shall be paid to him direct or to other person authorised by him
in this behalf.
Chapter: Wages
Section 50:
All wages shall be paid in current coin or currency or in both.
Chapter: Wages
Section 51: Wages to be paid without deduction.-
Wages shall be paid without any deduction of any kind except those permissible under Payment
of Wages Act, 1936 (4 of 1936).
Chapter: Wages
Section 52: Provisions of Rules 45 to 51 not to apply where other Acts apply
Nothing contained in Rules 45 to 51 shall apply to wages to contract labour in any
employment to which the Payment of Wages Act or the Minimum Wages Act, as the case may be,
apply and the time and conditions of payment of wages and deductions permissible from wages
in respect of workmen employed by the contractor in such employment shall be governed by the
provisions of Payment of Wages Act or the Minimum Wages Act, as the case may be and Rules
made thereunder.
Chapter: Wages
Section 53: Display of notice showing wage period etc
(1) A notice showing the wage period and the place and time of disbursement of wages shall
be displayed at the place of work and a copy sent by the contractor to the principal
employer under acknowledgement.
Chapter: Wages
Section 54: Entries regarding payment of wages, etc. to be made in register
(1) Entries denoting the time and place of payment of wages and the payments actually made
shall be made in the register of wages simultaneously as the payments are made.
The authorised representative of the principal employer shall affix his initials against
each entry and further record a certificate at the end of the entries in the following
form:-
"Certified that the amount shown in column No.. has been paid to the workman concerned in my
presence.
Chapter: Register and Records and Collection Statistics
Section 55: Register of contractors
Every principal employer shall maintain in respect of each registered establishment a
register of contractors in Form No. VIII
Chapter: Register and Records and Collection Statistics
Section 56: ****
****
Register of persons employed deleted 4.8.2000
Chapter: Register and Records and Collection Statistics
Section 57: Identity Card
(1) Every contractor shall issue an identity card in From No. X to each worker on the first
day of the employment of the worker.
(2) The contractor shall ensure that the worker carries his identity card with him when
employed on work.
(3) The card shall be maintained up-to-date and any change in the particulars entered
therein.
Chapter: Register and Records and Collection Statistics
Section 58: ****
****
deleted 4.8.2000
Service Certificate
Chapter: Register and Records and Collection Statistics
Section 59: Muster Roll, Wages, Registers, Deduction Register and Overtime Register
(1) In respect of establishments which are governed by the Payment of Wages Act and the
rules made thereunder, or the Minimum Wages Act and the rules made thereunder, the following
registers and records required to be maintained by the contractor as employed under those
Acts and the Rules made thereunder shall be deemed to be registers and records to be
maintained by the contractor under these rules:-
(a) Muster Roll;
(b) Register of Deductions;
(c) Register of Overtime;
(d) Register of Fines;
(e) Register of Advances.
(2) In respect of establishments not covered under sub-rule (1) the following provisions
shall apply, namely:-
(a) Every contractor shall maintain a register of muster roll cum wage-register in as
prescribed by sub-rule (1) of rule 27 of Maharashtra Minimum Wages Rules, 1963.
(b) Every contractor shall issue wage slips as prescribed by sub-rule (2) of rule 27 of
Maharashtra Minimum Wages Rules, 1963.;
(c) Signature or thumb-impression of every worker on the register of wages or wages-cummuster
roll as the case may be, shall be obtained and entries therein, shall be
authenticated by the initials of the contractor or his representative and duly certified by
the authorised representative of principal employer as required by rule 54;
Chapter: Register and Records and Collection Statistics
Section 60: Display of Act and Rules
Every contractor shall display an abstract of the Act and Rules in English, in Hindi or in
Marathi in such Form as may be approved by the Commissioner of Labour.
Chapter: Register and Records and Collection Statistics
Section 61: Register to be kept handy
(1) All registers and other records required to be maintained under the Act and Rules,
unless otherwise provided for, shall be kept at an office or the nearest convenient building
within the precincts of the work place or at a place within radious of three kilometers.
2. Such registers shall be maintained legibly in English, Hindi or Marathi.
3. All the registers and others records shall be preserved in margin for period of three
calendar years from the date of last entry therein.
4. All the registers records and notices maintained under the Act or Rules shall be produced
on demand before the Inspector or any other authority under the Act or any person authorised
in that behalf by the Government.
Chapter: Register and Records and Collection Statistics
Section 62: Notice showing rates of wages etc. to be displayed
(1) (i) Notices showing the wages, hours of work, hours of work, wage periods, date of
payment of wages, names and addresses of the inspectors having jurisdiction and date of
payment of unpaid wage shall be displayed in English, in Hindi or in Marathi in conspicuous
places at the establishment and the work site by the principal employer or the contractor as
the case may be.
(ii) The notices shall be correctly maintained in a clean and legible condition.
(2) A copy of the notices shall be sent to the Inspector and whenever any changes occur the
same shall be communicated to him forthwith..
(3) *****
Chapter: Register and Records and Collection Statistics
Section 63: Submission of Returns
(1) ***
(2) Every principal officer of a registered establishment shall send annually a return in
Form XXi (in duplicate) so as to reach the registering officer concerned not later than 15th
February following the end of the year to which it relates.
Chapter: Register and Records and Collection Statistics
Section 64: Power of Board, Committee, etc., to call for information
(1) The Board, Committee, Commissioner of Labour or the Inspector or any other authority
under the Act shall have powers to call for any information or statistics in relation to
contract labour form any contractor or principal employer at any time by an order in
writing.
(2) Any person called upon to furnish the information under sub-rule (1) shall be legally
bound to do so.